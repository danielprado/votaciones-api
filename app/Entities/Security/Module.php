<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modulo';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Modulo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Modulo', 'Redireccion', 'Imagen'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'Id_Modulo'         =>  'int',
        'Nombre_Modulo'     =>  'string',
        'Imagen'            =>  'string',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the activity's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Modulo;
    }

    /**
     * Get the activity name.
     *
     * @return int
     */
    public function getNameAttribute()
    {
        return $this->Nombre_Modulo;
    }

    /**
     * Get the activity name.
     *
     * @return int
     */
    public function getImageAttribute()
    {
        return $this->Imagen;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Module has many activities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany( ModuleActivities::class );
    }
}
