<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class ModuleActivities extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actividades';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Actividad';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Id_Modulo', 'Nombre_Actividad', 'Descripcion'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'Id_Actividad'      =>  'int',
        'Id_Modulo'         =>  'int',
        'Nombre_Actividad'  =>  'string',
        'Descripcion'       =>  'string',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the activity's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Actividad;
    }

    /**
     * Get the module's id.
     *
     * @return int
     */
    public function getModuleIdAttribute()
    {
        return (int) $this->Id_Modulo;
    }

    /**
     * Get the activity name.
     *
     * @return int
     */
    public function getNameAttribute()
    {
        return $this->Nombre_Actividad;
    }

    /**
     * Get the activity name.
     *
     * @return int
     */
    public function getDescriptionAttribute()
    {
        return $this->Descripcion;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Many users can have many activities
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function people()
    {
        return $this->belongsToMany( Profile::class, 'actividad_acceso',  'Id_Actividad', 'Id_Persona');
    }

    /**
     * Activities belongs to one module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo( Module::class, 'Id_Modulo' );
    }

}
