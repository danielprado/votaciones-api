<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class OauthSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oauth_sessions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id', 'owner_type', 'owner_id', 'client_redirect_uri'];


    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Refresh Token
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function refresh_token()
    {
        return $this->belongsTo( OauthRefreshToken::class, 'client_id');
    }

    /**
     * Access Token
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function access_token()
    {
        return $this->belongsTo( OauthAccessToken::class, 'client_id' );
    }
}
