<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actividad_acceso';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Id_Actividad', 'Estado'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator Attributes
    * ---------------------------------------------------------
    */

    /**
     * Get the user access id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Persona;
    }

    /**
     * Get the activity id.
     *
     * @return int
     */
    public function getActivityIdAttribute()
    {
        return (int) $this->Id_Actividad;
    }

    /**
     * Get the activity name.
     *
     * @return int
     */
    public function getActivityAttribute()
    {
        return isset( $this->activities->name ) ? $this->activities->name : null;
    }

    /**
     * Get the status.
     *
     * @return int
     */
    public function getStatusAttribute()
    {
        return (int) $this->Estado;
    }

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
     */

    /**
     * Check if an user is active into the module
     *
     * @param $query
     * @return mixed
     */
    public function scopeWhoHasAccess($query )
    {
        return $query->where('Estado', 1);
    }

    /**
     * Check if an user is inactive into the module
     *
     * @param $query
     * @return mixed
     */
    public function scopeWhoHasNotAccess($query)
    {
        return $query->where('Estado', 0);
    }

    public function scopeInThisModule( $query )
    {
        return $query->whereHas('activities', function ($query) {
            $query->where('Id_Modulo', env('MODULE_ID') );
        });
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    public function activities()
    {
        return $this->belongsTo( ModuleActivities::class, 'Id_Actividad' );
    }
}
