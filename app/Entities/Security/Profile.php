<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vista_datos_general_usuario';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Cedula', 'Primer_Apellido', 'Segundo_Apellido', 'Primer_Nombre', 'Segundo_Nombre', 'Fecha_Nacimiento', 'Nombre_Ciudad', 'Id_Pais', 'Id_TipoDocumento', 'Id_Pais', 'Id_Genero', 'Id_Etnia', 'i_fk_id_ciudad'];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name', 'part_name'];


    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the user's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Persona;
    }

    /**
     * Get user full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $n1 = isset( $this->Primer_Nombre ) ?     $this->Primer_Nombre : null;
        $n2 = isset( $this->Segundo_Nombre )    ? $this->Segundo_Nombre : null;
        $n3 = isset( $this->Primer_Apellido )   ? $this->Primer_Apellido : null;
        $n4 = isset( $this->Segundo_Apellido )  ? $this->Segundo_Apellido : null;

        $full_name = trim( $n1 ).' '.trim( $n2 ).' '.trim( $n3 ).' '.trim( $n4 );

        return preg_replace('/\s/', ' ', trim( $full_name ) );
    }

    /**
     * Get use part name
     *
     * @return string
     */
    public function getPartNameAttribute()
    {
        $n1 = isset( $this->Primer_Nombre ) ?     $this->Primer_Nombre : null;
        $n3 = isset( $this->Primer_Apellido )   ? $this->Primer_Apellido : null;

        $full_name = trim( $n1 ).' '.trim( $n3 );

        return preg_replace('/\s/', ' ', trim( $full_name ) );
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * User Credentials
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function access()
    {
        return $this->belongsTo(User::class, 'Id_Persona');
    }

    /**
     * Get the user permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany( Permissions::class, 'Id_Persona' );
    }
}
