<?php


namespace IDRDApp\Entities\Surveys;


use Illuminate\Database\Eloquent\Model;

class AnswersView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "survey_answers_view";
}