<?php

namespace IDRDApp\Entities\Surveys;

use Illuminate\Database\Eloquent\Model;

class OfferedAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'offered_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'answer', 'question_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'question_id'        =>  'int'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function people_answers()
    {
        return $this->belongsToMany(SurveyedPeople::class, 'people_answers','answer_id', 'surveyed_id')
                    ->withPivot('id', 'surveyed_id', 'answer_text')
                    ->withTimestamps();
    }
}
