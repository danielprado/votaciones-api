<?php

namespace IDRDApp\Entities\Surveys;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'question', 'input_name', 'question_type_id', 'survey_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'question_type_id'        =>  'int',
        'survey_id'               =>  'int'
    ];

    public function setInputNameAttribute($value)
    {
        $this->attributes['input_name'] = strtolower( $value );
    }

    public function question_type()
    {
        return $this->belongsTo(QuestionType::class, 'question_type_id');
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class, 'survey_id');
    }

    public function answers()
    {
        return $this->hasMany(OfferedAnswer::class, 'question_id', 'id');
    }
}
