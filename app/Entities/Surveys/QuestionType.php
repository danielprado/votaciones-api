<?php

namespace IDRDApp\Entities\Surveys;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'question_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'display_name', 'require_text' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'require_text'  =>  'boolean'
    ];
}
