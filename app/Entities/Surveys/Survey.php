<?php

namespace IDRDApp\Entities\Surveys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'pin', 'multiple_votes', 'available_from', 'available_until', 'results_at', 'time_between_questions' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'time_between_questions'        =>  'int',
        'results_at'        =>  'datetime',
        'available_from'    =>  'datetime',
        'available_until'   =>  'datetime',
        'multiple_votes'    =>  'boolean'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'results_at',
        'available_from',
        'available_until',
        'deleted_at',
    ];

    /**
     * Get the count of votes for specified campaign
     *
     * @return int
     */
    public function getCountVotesAttribute()
    {
        return  $this->questions()->withCount([
            'answers' => function($q) {
                return $q->withCount('people_answers');
            }
        ]);
    }


    public function questions()
    {
        return $this->hasMany(Question::class, 'survey_id', 'id');
    }
}
