<?php


namespace IDRDApp\Entities\Surveys;


use Illuminate\Database\Eloquent\Model;

class SurveyQuestionsView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "survey_questions_view";
}