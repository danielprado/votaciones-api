<?php

namespace IDRDApp\Entities\Surveys;

use Illuminate\Database\Eloquent\Model;

class SurveyedPeople extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveyed_people';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'document' ];


    public function people_answers()
    {
        return $this->belongsToMany(OfferedAnswer::class, 'people_answers','surveyed_id', 'answer_id')
            ->withPivot('id', 'answer_id', 'answer_text')
            ->withTimestamps();
    }
}
