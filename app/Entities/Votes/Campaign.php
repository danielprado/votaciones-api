<?php

namespace IDRDApp\Entities\Votes;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'campaigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'available_from', 'available_until', 'results_at' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'results_at'        =>  'datetime',
        'available_from'    =>  'datetime',
        'available_until'   =>  'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['count_votes'];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the count of votes for specified campaign
     *
     * @return int
     */
    public function getCountVotesAttribute()
    {
        return (int) $this->voters()->wherePivot('voted_at', '!=', null)->count();
    }

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
     */

    /**
     * Scope a query to only include active campaigns.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->whereDate('available_until', '>=', Carbon::now());
    }

    /**
     * Scope a query to only include inactive campaigns.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotActive($query)
    {
        return $query->where('available_until', '<=', Carbon::now());
    }

    /**
     * Scope a query to only include campaigns with votes.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereHasVotes($query )
    {
        return $query->whereHas( 'voters', function ($q) {
            return $q->whereNotNull( 'voted_at' );
        });
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Voters of the Campaigns
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function voters()
    {
        return $this->belongsToMany( Voter::class, 'voters_campaigns', 'campaign_id' )->withPivot('id', 'voted_at', 'candidate_id', 'voter_id');
    }

    /**
     * Candidates of the Campaigns
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidates()
    {
        return $this->hasMany( Candidate::class,  'campaign_id', 'id');
    }
}
