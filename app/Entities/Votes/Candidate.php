<?php

namespace IDRDApp\Entities\Votes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'candidates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'age', 'profile_pic', 'campaign_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'age' =>  'date'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image', 'co_image', 'campaign_name', 'count_votes'];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get quantity of votes by candidate
     *
     * @return int
     */
    public function getCountVotesAttribute()
    {
        return (int) $this->votes()->wherePivot('voted_at', '!=', null)->count();
    }

    /**
     * Get image url
     *
     * @return string|null
     */
    public function getImageAttribute()
    {
        return file_exists( public_path( 'storage/candidates' ).'/'. $this->profile_pic )
                ? asset( 'public/storage/candidates/'.$this->profile_pic )
                : null;
    }

    /**
     * Get image url
     *
     * @return string|null
     */
    public function getCoImageAttribute()
    {
        return isset($this->co_representative_profile_pic) && file_exists( public_path( 'storage/candidates' ).'/'. $this->co_representative_profile_pic )
            ? asset( 'public/storage/candidates/'.$this->co_representative_profile_pic )
            : null;
    }

    /**
     * Get image path
     *
     * @return string|null
     */
    public function getImagePathAttribute()
    {
        return public_path( 'storage/candidates' ).'/'. $this->profile_pic;
    }

    /**
     * Get the candidate campaign name
     *
     * @return string|null
     */
    public function getCampaignNameAttribute()
    {
        return isset( $this->campaign->name ) ? $this->campaign->name : null;
    }

    /**
     * Set the candidate's name in uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setCoRepresentativeNameAttribute($value)
    {
        $this->attributes['co_representative_name'] = toUpper($value);
    }


    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Candidate belongs to an unique campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo( Campaign::class, 'campaign_id' );
    }

    /**
     * Candidates belongs to many votes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votes()
    {
        return $this->belongsToMany( Voter::class, 'voters_campaigns', 'candidate_id' )->withPivot('id', 'voted_at', 'campaign_id', 'voter_id');
    }


    public function teams()
    {
        return $this->hasMany( Team::class, 'candidate_id', 'id' );
    }
}
