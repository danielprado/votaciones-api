<?php

namespace IDRDApp\Entities\Votes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityQuestion extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'security_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'security_question' ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Security Questions to Validate Votes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function security_questions()
    {
        return $this->belongsToMany( Voter::class, 'voters_security_questions', 'security_question_id' )->withPivot('voter_id', 'campaign_id', 'security_answer');
    }
}
