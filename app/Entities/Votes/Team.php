<?php


namespace IDRDApp\Entities\Votes;


use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'age', 'profile_pic', 'candidate_id' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'age' =>  'date'
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get image url
     *
     * @return string|null
     */
    public function getImageAttribute()
    {
        return file_exists( public_path( 'storage/candidates' ).'/'. $this->profile_pic )
            ? asset( 'public/storage/candidates/'.$this->profile_pic )
            : null;
    }

    /**
     * Get image path
     *
     * @return string|null
     */
    public function getImagePathAttribute()
    {
        return public_path( 'storage/candidates' ).'/'. $this->profile_pic;
    }
}