<?php

namespace IDRDApp\Entities\Votes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'document', 'sport', 'email' ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the candidate's name in uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = toUpper($value);
    }

    /**
     * Set the candidate's name in uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setSportAttribute($value)
    {
        $this->attributes['sport'] = toUpper($value);
    }

    /**
     * Set the candidate's name in uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = toLower($value);
    }

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
     */

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCanVote($query, $campaign_id)
    {
        return $query->whereHas('campaigns', function ($query) use ( $campaign_id ) {
            $query->where( 'campaign_id', $campaign_id)->whereNull('voted_at');
        });
    }

    /**
     * Scope a query to only include inactive users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCanNotVote($query, $campaign_id)
    {
        return $query->whereHas('campaigns', function ($query) use ( $campaign_id ) {
            $query->where( 'campaign_id', $campaign_id)->whereNotNull('voted_at');
        });
    }

    /**
     * Scope a query to only include users where has voted in a specific campaign.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasVote($query, $campaign_id)
    {
        return $query->with([
            'campaigns' => function ( $q ) use ( $campaign_id ) {
                return $q->where( 'campaign_id', $campaign_id)->whereNotNull('voted_at');
            }
        ]);
    }

    /**
     * Scope a query to only include users where has not voted in a specific campaign.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasNotVote($query, $campaign_id)
    {
        return $query->with([
            'campaigns' => function ($q) use ($campaign_id) {
                return $q->where('campaign_id', $campaign_id)->whereNull('voted_at');
            }
        ]);
    }

    /**
     * Scope a query to only include users where has been assigned in a specific campaign.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereCampaign($query, $campaign_id)
    {
        return $query->whereHas('campaigns', function ($query) use ( $campaign_id ) {
            $query->where( 'campaign_id', $campaign_id);
        });
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Campaigns Available to the Voters
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function campaigns()
    {
        return $this->belongsToMany( Campaign::class, 'voters_campaigns', 'voter_id' )->withPivot('id', 'voted_at', 'candidate_id', 'campaign_id');
    }

    /**
     * Security Questions to Validate Votes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function security_questions()
    {
        return $this->belongsToMany( SecurityQuestion::class, 'voters_security_questions', 'voter_id' )->withPivot('id', 'security_question_id', 'campaign_id', 'security_answer');
    }
}
