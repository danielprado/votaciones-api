<?php

namespace IDRDApp\Http\Controllers\Auth;

use IDRDApp\Http\Requests\Auth\AccessTokenRequest;
use IDRDApp\Http\Requests\Auth\UnlockRequest;
use IDRDApp\Transformers\Security\ProfileTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use IDRDApp\Entities\Security\OauthSession;
use IDRDApp\Http\Controllers\Controller;

class AccessTokenController extends Controller
{
    /**
     * Return token and user data
     *
     * @param AccessTokenRequest $request
     * @return JsonResponse
     */
    public function index(AccessTokenRequest $request)
    {
        return response()->json([
            'data'  =>  array_merge( Authorizer::issueAccessToken(), (new ProfileTransformer())->transform( request()->user()->profile ), ['username' => auth()->user()->username] ),
            'code'  => 200
        ], 200 );
    }

    public function lock()
    {
        if ( auth()->guest() ) {
            return response()->json([
                'message'   =>  trans('validation.handler.unauthenticated'),
                'code'      =>  401
            ], 401);
        }

        auth()->logout();

        return response()->json([
            'data'   =>  trans('validation.handler.locked'),
            'code'      =>  200
        ], 200);
    }

    public function unlock(UnlockRequest $request)
    {
        $verified = (new PasswordGrantVerifier())->verify( $request->get('username'), $request->get('password') );

        if ( $verified ) {
            $user_id = Authorizer::getResourceOwnerId();
            $client_id = Authorizer::getClientId();

            $session = OAuthSession::where('client_id', $client_id)->where('owner_id', $user_id)->first();

            if ( isset( $session->owner_id ) ) {
                return response()->json([
                    'data'   =>  trans('validation.handler.unlocked'),
                    'code'      =>  200
                ], 200);
            }
        }

        return response()->json([
            'message'   =>  trans('validation.handler.unauthenticated'),
            'code'      =>  401
        ], 401);
    }

    /**
     * Logout
     *
     * @return JsonResponse
     */
    public function logout()
    {
        $user_id = Authorizer::getResourceOwnerId();
        $client_id = Authorizer::getClientId();

        $sessions = OAuthSession::where('client_id', $client_id)->where('owner_id', $user_id)->get();

        foreach($sessions as $session)
        {
            foreach($session->access_token()->get() as $token)
            {
                Cache::forget($token->id);
            }
            $session->refresh_token()->delete();
            $session->delete();
        }

        auth()->logout();

        return response()->json([
            'data'  =>  trans('validation.handler.logout'),
            'code'  =>  200
        ], 200);
    }
}
