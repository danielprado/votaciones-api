<?php

namespace IDRDApp\Http\Controllers\Auth;



use IDRDApp\Entities\Security\Permissions;
use Illuminate\Support\Facades\Auth;
use IDRDApp\Entities\Security\User;

class PasswordGrantVerifier
{
    /**
     * Verify Oauth users
     *
     * @param $username
     * @param $password
     * @return bool
     */
    public function verify($username, $password)
    {

        $user = User::query()->where([
            ['Usuario', $username],
            ['Contrasena', $this->hash( $password )],
        ])->first();

        //Check if query return an user
        $user_exists = isset( $user->id ) ? $user->id : null;
        //Abort if credentials were wrong
        abort_if( !$user_exists, 422, trans('auth.failed') );

        //If user exists check if has access to the module
        $access = Permissions::query()->whereHas('activities', function ($query) {
            $query->where('Id_Modulo', env('MODULE_ID') );
        })->find( $user_exists )->pluck('Estado', 'Id_Actividad')->toArray();

        abort_if( count( $access ) < 1, 403, trans('validation.handler.unauthorized') );

        if ( Auth::loginUsingId( $user->id ) ) {
            return Auth::user()->id;
        }

        return false;
    }

    /**
     * Hash the password
     *
     * @param $password
     * @return string
     */
    public function hash( $password )
    {
        $k = 18;
        $C = '';
        for( $i=0; $i < strlen( $password ) ; $i++ ) $C.=chr((ord( $password [$i])+$k)%255);
        return sha1( $C );
    }

    /**
     * Decrypt password
     *
     * @param $password
     * @return string
     */
    public function decrypt($password)
    {
        $k = 18;
        $M = '';
        for($i=0; $i<strlen($password); $i++)$M.=chr((ord($password[$i])-$k+255)%255);
        return $M;
    }
}
