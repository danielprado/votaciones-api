<?php

namespace IDRDApp\Http\Controllers\Landing;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Transformers\Votes\CampaignTransformer;
use IDRDApp\Transformers\Votes\CandidateTransformer;
use IDRDApp\Transformers\Votes\LandingCampaignTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->table( new Campaign(), new LandingCampaignTransformer(), false);
        $data = $data->where('available_from', '<=', Carbon::now()->format('Y-m-d H:i:s') )->has('candidates');
        $data = $this->paginateCollection( $data, new CampaignTransformer() );

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }


    public function next()
    {
        $campaigns = Campaign::query()->where('available_from', '>', Carbon::now()->format('Y-m-d H:i:s') )
                                      ->has('candidates')->get();

        $resource = new Collection($campaigns, new LandingCampaignTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Campaign $campaign)
    {
        if ( $campaign->has('candidates') ) {
            $resource = new Item($campaign, new LandingCampaignTransformer());
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json($rootScope->toArray(), 200);
        }

        return  $this->error_response( trans('validation.handler.resource_not_found'), 404 );
    }

    public function candidate( Campaign $campaign, Candidate $candidate )
    {

        if ( $campaign->id == $candidate->campaign_id ) {
            $resource = new Item($candidate, new CandidateTransformer());
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json($rootScope->toArray(), 200);
        }

        return  $this->error_response( trans('validation.handler.resource_not_found'), 404 );
    }
}
