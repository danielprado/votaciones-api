<?php


namespace IDRDApp\Http\Controllers\Landing;


use Carbon\Carbon;
use IDRDApp\Entities\Surveys\AnswersView;
use IDRDApp\Entities\Surveys\Question;
use IDRDApp\Entities\Surveys\QuestionType;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Entities\Surveys\SurveyedPeople;
use IDRDApp\Entities\Surveys\SurveyQuestionsView;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Surveys\StorePeopleAnswersRequest;
use IDRDApp\Transformers\Surveys\QuestionTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PeopleAnswerController extends Controller
{
    public function index($id)
    {
        abort_if( !isset($id), 404, trans('validation.handler.resource_not_found') );

        $questions = Question::query()->has('answers')->where('survey_id', $id)->get();
        $resource = new Collection($questions, new QuestionTransformer());
        $manager = new Manager();
        $questions = $manager->createData($resource)->toArray()['data'];


        $data = collect( $questions )->map(function ($question) {
            if ( $question['require_text'] == true &&  $question['question_type_name'] != 'boolean' ) {
                $people_answers  =  collect( $question['answers'] )->map(function ( $answer ) {
                    $answers = AnswersView::query()->where('answer_id', $answer['id'])->get();
                    $array = [];
                    foreach ($answers as $data) {
                        $array[] = [
                            'id'            =>  isset( $data['id'] ) ? $data['id'] : null,
                            'name'          =>  isset( $data['name'] ) ? $data['name'] : null,
                            'document'      =>  isset( $data['document'] ) ? $data['document'] : null,
                            'answer'        =>  isset( $data['answer_text'] ) ? $data['answer_text'] : null,
                            'created_at'    =>  isset( $data['created_at'] ) ? Carbon::parse($data['created_at'])->format('Y-m-d H:i:s') : null,
                        ];
                    }
                    return [
                        'id'                =>  $answer['id'],
                        'sub_question'      =>  $answer['answer'],
                        'count_answers'     =>  $answer['count_answers'],
                        'people_answers'    =>  $array,
                    ];
                })->toArray();
                return [
                    'id'            => $question['id'],
                    'question'      => $question['question'],
                    'question_type_name'    => $question['question_type_name'],
                    'require_text'  => $question['require_text'],
                    'answers'       => $people_answers
                ];
            } else {
                $people_answers  =  collect( $question['answers'] )->map(function ( $answer ) {
                    $answers = AnswersView::query()->where('answer_id', $answer['id'])->get();
                    $array = [];
                    $count_true = 0;
                    $count_false = 0;
                    foreach ($answers as $data) {

                        if ( $data['question_type_name'] == 'boolean') {
                            if ( (boolean) $data['answer_text'] == true) {
                                $count_true++;
                            }
                            if ( (boolean) $data['answer_text'] == false) {
                                $count_false++;
                            }
                            $array[] = [
                                'id'            =>  isset( $data['id'] ) ? $data['id'] : null,
                                'name'          =>  isset( $data['name'] ) ? $data['name'] : null,
                                'document'      =>  isset( $data['document'] ) ? $data['document'] : null,
                                'sub_question'  =>  isset( $data['answer'] ) ?  $data['answer'] : null,
                                'answer'        =>  isset( $data['answer_text'] ) ? (boolean) $data['answer_text'] : null,
                                'created_at'    =>  isset( $data['created_at'] ) ? Carbon::parse($data['created_at'])->format('Y-m-d H:i:s') : null,
                            ];
                        } else {
                            $array[] = [
                                'id'            =>  isset( $data['id'] ) ? $data['id'] : null,
                                'name'          =>  isset( $data['name'] ) ? $data['name'] : null,
                                'document'      =>  isset( $data['document'] ) ? $data['document'] : null,
                                'sub_question'  =>  isset( $data['answer'] ) ? $data['answer'] : null,
                                'created_at'    =>  isset( $data['created_at'] ) ? Carbon::parse($data['created_at'])->format('Y-m-d H:i:s') : null,
                            ];
                        }
                    }
                    return [
                        'id'                =>  $answer['id'],
                        'sub_question'      =>  $answer['answer'],
                        'count_answers'     =>  $answer['count_answers'],
                        'count_true'        =>  $count_true,
                        'count_false'       =>  $count_false,
                        'people_answers'    =>  $array,
                    ];
                })->toArray();
                if ( $question['question_type_name'] == 'boolean' ) {
                    $true = isset( $people_answers[0]['count_true'] ) ? $people_answers[0]['count_true'] : 0;
                    $false = isset( $people_answers[0]['count_false'] ) ? $people_answers[0]['count_false'] : 0;
                    return [
                        'id'            => $question['id'],
                        'question'      => $question['question'],
                        'question_type_name'    => $question['question_type_name'],
                        'require_text'  => $question['require_text'],
                        'answers'       => $people_answers,
                        'graphic_data'      =>  [
                            'data'  =>  [
                                'labels'    =>      [ 'Verdadero', 'Falso' ],
                                'series'    =>  [  [ $true, $false ] ]
                            ],
                            'options'   =>  [
                                'axisX' => [
                                    'showGrid' => false
                                ],
                                'low'   =>  0,
                                'high'  =>  (int) max( [$true, $false] ) + 1,
                                'chartPadding'  =>  [
                                    'top'  =>  0,
                                    'right'  =>  5,
                                    'bottom'  =>  0,
                                    'left'  =>  0,
                                ],
                            ],
                        ],
                    ];
                } else {
                    return [
                        'id'            => $question['id'],
                        'question'      => $question['question'],
                        'question_type_name'    => $question['question_type_name'],
                        'require_text'  => $question['require_text'],
                        'answers'       => $people_answers,
                        'graphic_data'      =>  [
                            'data'  =>  [
                                'labels'    =>      array_pluck($people_answers, 'sub_question'),
                                'series'    =>  [    array_pluck( $people_answers, 'count_answers' ) ]
                            ],
                            'options'   =>  [
                                'axisX' => [
                                    'showGrid' => false
                                ],
                                'low'   =>  0,
                                'high'  =>  (int) max( array_pluck( $people_answers, 'count_answers' ) ) + 1,
                                'chartPadding'  =>  [
                                    'top'  =>  0,
                                    'right'  =>  5,
                                    'bottom'  =>  0,
                                    'left'  =>  0,
                                ],
                            ],
                        ],
                    ];
                }
            }
        })->toArray();

        return response()->json([
            'data'   =>  $data,
            'total'  =>   AnswersView::query()->where('survey_id', $id)->count( DB::raw('DISTINCT surveyed_id') ),
            'code'  =>  200
        ], 200);
    }

    public function store(StorePeopleAnswersRequest $request)
    {
        $survey =  Survey::query()->where('id', $request->get('survey_id') )->first();

        if ( isset( $survey->id ) ) {

            if ( ! $survey->multiple_votes ) {
                $has_votes = AnswersView::query()->where('surveyed_id', $request->get('surveyed_id'))
                                                 ->where('survey_id', $request->get('survey_id'))
                                                ->count();


                if ( $has_votes > 0 ) {
                    return $this->error_response(trans('validation.handler.survey_answered'));
                }
            }

            if ( $survey->available_until <= Carbon::now()->format('Y-m-d H:i:s') ) {
                return $this->error_response(trans('validation.handler.survey_closed'));
            }

            $questions = SurveyQuestionsView::query()
                    ->where('id', $request->get('survey_id') )->get();

            $requested = $request->get('answers');


            foreach ($questions as $question) {
                if ( strpos( $question->name, 'required' ) ) {

                    $check = array_where( $requested, function ($key, $value) use ( $question ) {
                        return ( is_int($value['answer_id'] ) && (int) $value['answer_id'] == (int) $question->answer_id ) || ( is_array( $value['answer_id'] ) && count($value['answer_id']) > 0 );
                    });

                    if ( $question->name == "options_required" ) {
                        $check = array_where( $requested, function ($key, $value) use ( $question ) {
                            return is_int( $value['answer_id'] );
                        });
                        if ( count( $check ) < 1 ) {
                            return $this->error_response( trans('validation.handler.complete_answers') );
                        }
                    } else {
                        if ( count( $check ) < 1 ) {
                            return $this->error_response( trans('validation.handler.complete_answers') );
                        }
                    }



                    if ( (boolean) $question->require_text ) {

                        $check = array_where( $requested, function ($key, $value) use ( $question ) {
                            return ( is_int($value['answer_id']) && $value['answer_id'] == $question->answer_id ) && ( is_null( $value['value'] ) || $value['value'] == ''  ||  is_array( $value['answer_id'] ) && count($value['answer_id']) == 0 );
                        });


                        if ( count( $check ) > 0 ) {
                            return $this->error_response(trans('validation.handler.complete_answers'));
                        }
                    }
                }
            }

            $data = [];
            foreach ( $request->get('answers') as $answer ) {
                if ( isset( $answer['answer_id'] ) && is_int( $answer['answer_id'] ) ) {
                    $data[] = [
                        'surveyed_id'   => $request->get('surveyed_id'),
                        'answer_id'     => (int) $answer['answer_id'],
                        'answer_text'   =>  isset( $answer['value'] ) ? $answer['value'] : null
                    ];
                } elseif (isset( $answer['answer_id'] ) && is_array( $answer['answer_id'] )) {
                    foreach ( $answer['answer_id'] as $id ) {
                        $data[] = [
                            'surveyed_id'   => $request->get('surveyed_id'),
                            'answer_id'     => (int) $id,
                            'answer_text'   =>  null
                        ];
                    }
                }
            }

            $person = SurveyedPeople::query()->where('id', $request->get('surveyed_id'))->first();
            if ( $person ) {
                $person->people_answers()->attach($data);
                return response()->json([
                    'data'  =>  trans('validation.handler.success'),
                    'code'  =>  201
                ], 201);
            }
            return $this->error_response(trans('validation.handler.unexpected_failure'));
        }

        return $this->error_response(trans('validation.handler.resource_not_found'));
    }
}