<?php


namespace IDRDApp\Http\Controllers\Landing;


use IDRDApp\Entities\Surveys\AnswersView;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Entities\Surveys\SurveyedPeople;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Surveys\StorePeopleRequest;

class PeopleController extends Controller
{
    public function store(StorePeopleRequest $request)
    {
        $person = SurveyedPeople::query()
                                ->where('document', $request->get('document'))
                                ->first();
        if ( isset( $person->id ) ) {
            $survey =  Survey::query()->where('id', $request->get('survey_id') )->first();
            if ( isset( $survey->id ) ) {
                if (!$survey->multiple_votes) {
                    $has_votes = AnswersView::query()->where('surveyed_id', $person->id)
                        ->where('survey_id', $request->get('survey_id'))
                        ->count();

                    if ( $has_votes > 0 ) {
                        return $this->error_response(trans('validation.handler.survey_answered'));
                    }
                }
            }

            return response()->json([
                'data'      =>  trans('validation.handler.success'),
                'person'    =>  $person
            ], 200);
        }

        $person = new SurveyedPeople();
        $person->name       =   $request->get('name');
        $person->document   =   $request->get('document');
        if ( $person->saveOrFail() ) {
            return response()->json([
                'data'      =>  trans('validation.handler.success'),
                'person'    =>  $person
            ], 200);
        }
    }
}