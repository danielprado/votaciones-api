<?php


namespace IDRDApp\Http\Controllers\Landing;


use Carbon\Carbon;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Surveys\ValidateSurveyRequest;
use IDRDApp\Transformers\Surveys\QuestionTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class QuestionController extends Controller
{
    public function validateSurvey(ValidateSurveyRequest $request, Survey $survey)
    {
        if ( Carbon::now()->greaterThan( $survey->available_until ) ) {
            return $this->error_response( trans('validation.handler.survey_closed') );
        }

        if ( Carbon::now()->lessThan( $survey->available_from ) ) {
            return $this->error_response( trans('validation.handler.survey_not_opened') );
        }
        /*
        if( $request->get('pin') != $survey->pin) {
            return $this->error_response( trans('validation.handler.pin_wrong') );
        }
        */

        $resource = new Collection($survey->questions, new QuestionTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json([
            'data'  =>  $rootScope->toArray()['data'],
            'code'  =>  200
        ], 200);
    }
}