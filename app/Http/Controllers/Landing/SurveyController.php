<?php


namespace IDRDApp\Http\Controllers\Landing;


use Carbon\Carbon;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Surveys\LandingSurveyTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->table( new Survey(), new LandingSurveyTransformer(), false);
        $data = $data->where('available_from', '<=', Carbon::now()->format('Y-m-d H:i:s') )->has('questions');
        $data = $this->paginateCollection( $data, new LandingSurveyTransformer() );

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }


    public function next()
    {
        $surveys = Survey::query()->where('available_from', '>', Carbon::now()->format('Y-m-d H:i:s') )
            ->has('questions')->get();

        $resource = new Collection($surveys, new LandingSurveyTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Survey $survey
     * @return JsonResponse
     */
    public function show(Survey $survey)
    {
        if ( $survey->has('questions') ) {
            $resource = new Item($survey, new LandingSurveyTransformer());
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json($rootScope->toArray(), 200);
        }

        return  $this->error_response( trans('validation.handler.resource_not_found'), 404 );
    }
}