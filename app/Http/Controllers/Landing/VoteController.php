<?php

namespace IDRDApp\Http\Controllers\Landing;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Voter;
use IDRDApp\Jobs\ConfirmVoteMail;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VoteController extends Controller
{
    /**
     * Check if requested user can vote or not
     *
     * @param Requests\Votes\ValidateVoterRequest $request
     * @param Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateVoter(Requests\Votes\ValidateVoterRequest $request, Campaign $campaign )
    {
        if ( Carbon::now()->greaterThan( $campaign->available_until ) ) {
            return $this->error_response( trans('validation.handler.campaign_closed') );
        }

        $requestedVoter = Voter::where('document', $request->get('document'))->firstOrFail();

        if ( isset( $requestedVoter->id ) ) {
            $voter = $campaign->voters()->wherePivot( 'voted_at', null )
                                        ->wherePivot('voter_id', $requestedVoter->id)->first();

            if ( isset( $voter->id ) ) {
                $data = Voter::find( $voter->id );
                $questions = $data->security_questions()->wherePivot('voter_id', $requestedVoter->id)
                                                        ->wherePivot('campaign_id', $campaign->id)
                                                            ->get();

                $response = [];

                foreach ( $questions as $question ) {

                    $random_answers = DB::table('voters_security_questions')
                                        ->where( 'voter_id', '!=', $voter->id  )
                                        ->where( 'campaign_id', $campaign->id  )
                                        ->where( 'security_question_id', $question->id  )
                                        ->inRandomOrder()->take( 5 )->get();
                    $answers = [];

                    foreach ( $random_answers as $answer ) {
                        $answers[] = [
                          'id'      => isset( $question->id ) ? $question->id : null,
                          'answer'  => isset( $answer->security_answer ) ? $answer->security_answer : null
                        ];
                    }

                    $answer = [
                        'id'      =>  isset( $question->id ) ? $question->id : null,
                        'answer'  =>  isset( $question->pivot->security_answer ) ? "{$question->pivot->security_answer}" : null
                    ];

                    $array = array_prepend( $answers, $answer );
                    shuffle( $array );

                    $response[] = [
                        'id'        =>  isset( $question->id ) ? $question->id : null,
                        'question'  =>  isset( $question->security_question ) ? $question->security_question : null,
                        'answers'   =>  $array,
                    ];
                }

                return response()->json([
                    'voter_id'  =>  $voter->id,
                    'name'      =>  isset( $voter->name ) ? $voter->name : null,
                    'data'  =>  $response,
                    'code'  =>  200
                ], 200);
            }

            return $this->error_response( trans('validation.handler.not_vote_twice') );
        }
    }

    /**
     * Validate answers to all the questions
     *
     * @param Requests\Votes\ValidateAnswersRequest $request
     * @param Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateAnswers(Requests\Votes\ValidateAnswersRequest $request, Campaign $campaign )
    {
        if ( Carbon::now()->greaterThan( $campaign->available_until ) ) {
            return $this->error_response( trans('validation.handler.campaign_closed') );
        }

        $voter = Voter::findOrFail( $request->get('voter_id') );

        if ( isset( $voter->id ) ) {
            $answers = 0;
            if ( count( $request->get('selected_answers') ) > 0 ) {
                foreach ( $request->get('selected_answers') as $answer) {
                    $question_id = isset( $answer['id'] ) ? $answer['id'] : null;
                    $voter_answer = isset( $answer['answer'] ) ? $answer['answer'] : null;
                    $questions = $voter->security_questions()
                        ->wherePivot('security_question_id', $question_id)
                        ->wherePivot('campaign_id', $campaign->id)
                        ->wherePivot('security_answer', $voter_answer)
                        ->wherePivot('voter_id', $voter->id)
                        ->first();
                    if ( isset( $questions->id ) ) {
                        $answers++;
                    }
                }

                if ( $answers ==  count( $request->get('selected_answers') ) ) {
                    return response()->json([
                        'data'  =>  trans( 'validation.handler.voter_validated' ),
                        'code'  =>  200
                    ], 200);
                } else {
                    // return $this->error_response( trans('validation.handler.voter_invalidated') );
                    return $this->error_response( 'Tu respuesta es incorrecta, si necesitas ayuda por favor comunícate con el Área de Talento Humano.' );
                }

            } else {
                return $this->error_response( trans('validation.handler.complete_answers') );
            }

        } else {
            return $this->error_response( trans('validation.handler.complete_answers') );
        }
    }

    /**
     * Store vote
     *
     * @param Request $request
     * @param Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function vote(Requests\Votes\ValidateVoteRequest $request, Campaign $campaign )
    {
        if ( Carbon::now()->greaterThan( $campaign->available_until ) ) {
            return $this->error_response( trans('validation.handler.campaign_closed') );
        }

        $query = $campaign->voters()->wherePivot('voter_id', $request->get('voter_id'))
                                     ->wherePivot('voted_at', null);

        $vote = $query->first();

         if ( isset( $vote->pivot->id ) ) {
             DB::table('voters_campaigns')->where( 'id',  $vote->pivot->id)
                                          ->update([
                                              'candidate_id' => $request->get( 'candidate_id' ),
                                              'voted_at'  =>  Carbon::now()
                                          ]);

             try {
                 $this->dispatch(new ConfirmVoteMail($vote, $campaign));
             } catch (\Exception $e) {}

             return response()->json([
                 'data'  =>  trans('validation.handler.voted_successful'),
                 'code'  =>  200
             ], 200);
         }

         return $this->error_response( trans('validation.handler.voted_unsuccessful') );

    }

    /**
     * @param $voter
     * @param Campaign $campaign
     */
    public function reconfirm($voter, Campaign $campaign)
    {
        $query = $campaign->voters()->wherePivot('voter_id', $voter)
            ->wherePivot('voted_at', '!=', null);
        $vote = $query->first();
        if ( isset( $vote->pivot->id ) ) {
            $this->dispatch(new ConfirmVoteMail($vote, $campaign));
            return response()->json([
                'data'  =>  trans('validation.handler.voted_successful'),
                'details' => $vote,
                'campaign' => $campaign,
                'code'  =>  200
            ], 200);
        }
        return $this->error_response( trans('validation.handler.voted_unsuccessful') );
    }
}
