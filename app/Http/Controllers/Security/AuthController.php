<?php

namespace IDRDApp\Http\Controllers\Security;

use Illuminate\Http\Request;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Security\UserTransformer;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return UserTransformer
     */
    public function index()
    {
        $resource = new Item( \request()->user(), new UserTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }
}
