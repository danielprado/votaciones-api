<?php


namespace IDRDApp\Http\Controllers\Surveys;


use IDRDApp\Entities\Surveys\OfferedAnswer;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Surveys\StoreOfferedAnswerRequest;
use IDRDApp\Transformers\Surveys\AnswersTransformer;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class OfferedAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new OfferedAnswer(), new AnswersTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOfferedAnswerRequest $request
     * @return Response
     * @throws \Throwable
     */
    public function store(StoreOfferedAnswerRequest $request)
    {
        $resource = new OfferedAnswer();
        $resource->answer               =   $request->get('answer');
        $resource->question_id       =   $request->get('question_id');

        if ( $request->hasFile('image') ) {
            $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('image')->getClientOriginalExtension();
            if ( $request->file('image')->move( public_path('storage/surveys'), $file_name  ) ) {
                $resource->path       =   $file_name;
                $resource->has_image  =   true;
            }
        }

        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param OfferedAnswer $answer
     * @return Response
     */
    public function show(OfferedAnswer $answer)
    {
        $resource = new Item($answer, new AnswersTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreOfferedAnswerRequest $request
     * @param OfferedAnswer $answer
     * @return Response
     * @throws \Throwable
     */
    public function update(StoreOfferedAnswerRequest $request, OfferedAnswer $answer)
    {
        $answer->answer          =   $request->get('answer');
        $answer->question_id     =   $request->get('question_id');

        if ( $request->hasFile('image') ) {
            $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('image')->getClientOriginalExtension();
            if ( $request->file('image')->move( public_path('storage/surveys'), $file_name  ) ) {
                $answer->path       =   $file_name;
                $answer->has_image  =   true;
            }
        }

        if ( $answer->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.updated'),
                'code'  => 200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param OfferedAnswer $answer
     * @return Response
     * @throws \Exception
     */
    public function destroy(OfferedAnswer $answer)
    {
        if ( $answer->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}