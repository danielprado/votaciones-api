<?php


namespace IDRDApp\Http\Controllers\Surveys;


use Carbon\Carbon;
use IDRDApp\Entities\Surveys\Question;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Surveys\StoreQuestionRequest;
use IDRDApp\Transformers\Surveys\QuestionTransformer;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new Question(), new QuestionTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreQuestionRequest $request
     * @return Response
     * @throws \Throwable
     */
    public function store(StoreQuestionRequest $request)
    {
        $resource = new Question();
        $resource->question               =   $request->get('question');
        $resource->question_type_id       =   $request->get('question_type_id');
        $resource->survey_id              =   $request->get('survey_id');
        $resource->input_name             =   $request->get('name');
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return Response
     */
    public function show(Question $question)
    {
        $resource = new Item($question, new QuestionTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreQuestionRequest $request
     * @param Question $question
     * @return Response
     * @throws \Throwable
     */
    public function update(StoreQuestionRequest $request, Question $question)
    {
        $question->question               =   $request->get('question');
        $question->question_type_id       =   $request->get('question_type_id');
        $question->survey_id              =   $request->get('survey_id');
        $question->input_name             =   $request->get('name');

        if ( $question->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.updated'),
                'code'  => 200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return Response
     * @throws \Exception
     */
    public function destroy(Question $question)
    {
        $question->answers()->delete();
        if ( $question->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}