<?php


namespace IDRDApp\Http\Controllers\Surveys;


use IDRDApp\Entities\Surveys\QuestionType;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Surveys\QuestionTypeTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class QuestionTypeController extends Controller
{
    public function index()
    {
        $resource = new Collection(QuestionType::all(), new QuestionTypeTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }
}