<?php

namespace IDRDApp\Http\Controllers\Surveys;

use Carbon\Carbon;
use Exception;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Transformers\Surveys\SurveyTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Throwable;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new Survey(), new SurveyTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the active resources.
     *
     * @return Response
     */
    public function active()
    {
        $surveys = Survey::query()->where('available_until', '>=', Carbon::now()->format('Y-m-d H:i:s'))->get();
        $resource = new Collection($surveys, new SurveyTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Surveys\StoreSuveyRequest $request
     * @return Response
     * @throws Throwable
     */
    public function store(Requests\Surveys\StoreSuveyRequest $request)
    {
        $resource = new Survey();
        $resource->name              =   $request->get('name');
        $resource->description       =   $request->get('description');
        $resource->available_from    =   $request->get('available_from');
        $resource->available_until   =   $request->get('available_until');
        $resource->results_at        =   $request->get('results_at');
        $resource->pin               =   $request->get('pin');
        $resource->multiple_votes    =   (boolean) $request->get('multiple_votes');
        $resource->time_between_questions        =   (int) $request->get('time_between_questions');
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Survey $survey
     * @return Response
     */
    public function show(Survey $survey)
    {
        $resource = new Item($survey, new SurveyTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Surveys\StoreSuveyRequest $request
     * @param Survey $survey
     * @return Response
     * @throws Throwable
     */
    public function update(Requests\Surveys\UpdateSuveyRequest $request, Survey $survey)
    {
        $survey->name         =   $request->get('name');
        $survey->description  =   $request->get('description');
        $survey->available_from    =   $request->get('available_from');
        $survey->available_until   =   $request->get('available_until');
        $survey->results_at   =   $request->get('results_at');
        $survey->pin   =   $request->get('pin');
        $survey->time_between_questions        =   (int) $request->get('time_between_questions');
        $survey->multiple_votes    =   (boolean) $request->get('multiple_votes');

        if ( $survey->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.updated'),
                'code'  => 200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Survey $survey
     * @return Response
     * @throws Exception
     */
    public function destroy(Survey $survey)
    {
        if ( $survey->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}
