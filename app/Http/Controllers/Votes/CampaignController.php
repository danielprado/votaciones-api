<?php

namespace IDRDApp\Http\Controllers\Votes;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Transformers\Votes\CampaignTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;
use Throwable;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new Campaign(), new CampaignTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the active resources.
     *
     * @return Response
     */
    public function active()
    {
        $campaigns = Campaign::query()->where('available_until', '>=', Carbon::now()->format('Y-m-d H:i:s'))->get();
        $resource = new Collection($campaigns, new CampaignTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Votes\StoreCampaignRequest $request
     * @return Response
     * @throws Throwable
     */
    public function store(Requests\Votes\StoreCampaignRequest $request)
    {
        $resource = new Campaign();
        $resource->name              =   $request->get('name');
        $resource->description       =   $request->get('description');
        $resource->available_from    =   $request->get('available_from');
        $resource->available_until   =   $request->get('available_until');
        $resource->results_at        =   $request->get('results_at');

        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Campaign $campaign
     * @return Response
     */
    public function show(Campaign $campaign)
    {
        $resource = new Item($campaign, new CampaignTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Votes\StoreCampaignRequest $request
     * @param Campaign $campaign
     * @return Response
     * @throws Throwable
     */
    public function update(Requests\Votes\StoreCampaignRequest $request, Campaign $campaign)
    {
        $campaign->name         =   $request->get('name');
        $campaign->description  =   $request->get('description');
        $campaign->available_from    =   $request->get('available_from');
        $campaign->available_until   =   $request->get('available_until');
        $campaign->results_at   =   $request->get('results_at');

        if ( $campaign->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.updated'),
                'code'  => 200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function destroy(Campaign $campaign)
    {
        if ( $campaign->count_votes > 0 ) {
            return $this->error_response( trans('validation.handler.campaign_not_deleted') );
        }
        $campaign->voters()->chunk(100, function ($voters) use ( $campaign ) {
            foreach ( $voters as $voter ) {
                $voter->security_questions()->wherePivot('campaign_id', $campaign->id)->detach();
            }
        });
        $campaign->voters()->detach();
        $campaign->candidates()->chunk( 10, function ($candidates) {
            foreach ( $candidates as $candidate ) {
                $candidate->delete();
            }
        });
        if ( $campaign->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }

    public function excel(Campaign $campaign)
    {
        Excel::load( public_path('files/REPORTE_CAMPANA.xlsx'), function ($file) use ( $campaign ) {
            $file->sheet(0, function ($sheet) use ( $campaign ) {
                $sheet->cell("A1", function($cell) use ($campaign) {
                    $cell->setValue( isset(  $campaign->name ) ?  $campaign->name : 'CAMPAÑA' );
                });
                $sheet->cell("A2", function($cell) {
                    $cell->setValue( "REPORTE GENERADO: ".Carbon::now()->format('Y-m-d H:i:s') );
                });
                $sheet->cell("A3", function($cell) use ($campaign) {
                    $date = isset( $campaign->available_from) ? $campaign->available_from->format('Y-m-d H:i:s') : '';
                    $cell->setValue( "FECHA DE APERTURA: {$date}" );
                });
                $sheet->cell("C3", function($cell) use ($campaign) {
                    $date = isset( $campaign->available_until) ? $campaign->available_until->format('Y-m-d H:i:s') : '';
                    $cell->setValue( "FECHA DE CIERRE: {$date}" );
                });
                $sheet->cell("A5", function($cell) use ($campaign) {
                    $cell->setValue( isset( $campaign->description) ? $campaign->description : '' );
                });

                $row = 10;
                $votes = 0;

                if ( isset( $campaign->candidates ) ) {
                    foreach ( $campaign->candidates as $candidate ) {
                        $teams = '';
                        if ( isset( $candidate->teams ) ) {
                            foreach ( $candidate->teams as $team ) {
                                $teams .= isset( $team->name ) ? "{$team->name}, " : null;
                            }
                        }
                        $count = isset( $candidate->count_votes ) ? (int) $candidate->count_votes : 0;
                        $data = [
                            'id'             =>  isset( $candidate->id ) ? (int) $candidate->id : null,
                            'name'           =>  isset( $candidate->name ) ? $candidate->name : null,
                            'team'           =>  $teams,
                            'count_votes'    =>  $count,
                        ];
                        $sheet->row($row, $data);
                        $row++;
                        $votes += $count;
                    }
                }

                $sheet->cell("D7", function($cell) use ($votes) {
                    $cell->setValue( $votes );
                });

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A9:D$sum")->applyFromArray($styleArray);
            });

            $file->sheet(1, function ($sheet) use ( $campaign ) {
                $sheet->cell("A1", function($cell) use ($campaign) {
                    $cell->setValue( isset(  $campaign->name ) ?  $campaign->name : 'CAMPAÑA' );
                });
                $sheet->cell("A3", function($cell)  {
                    $cell->setValue( "REPORTE GENERADO: ".Carbon::now()->format('Y-m-d H:i:s') );
                });

                $row = 6;

                if ( isset( $campaign->voters ) ) {
                    foreach ( $campaign->voters as $voter ) {
                        $data = [
                            'id'            =>  isset( $voter->id ) ? (int) $voter->id : null,
                            'name'          =>  isset( $voter->name ) ? $voter->name : null,
                            'sport'         =>  isset( $voter->sport ) ? $voter->sport : null,
                            'voted_at'      =>  isset( $voter->pivot->voted_at ) ? $voter->pivot->voted_at : null,
                            'candidate'     =>  null,// isset( $voter->pivot->candidate_id ) ? $this->includeCandidateName(  $voter->pivot->candidate_id ) : null,
                        ];

                        $sheet->row($row, $data);
                        $row++;
                    }
                }


                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A5:D$sum")->applyFromArray($styleArray);
            });

        })->export('xlsx');
    }

    public function includeCandidateName( $id = null )
    {
        if ( !is_null( $id ) ) {
            $candidate = Candidate::query()->where('id', $id)->first();
            return isset( $candidate->name ) ? $candidate->name : null;
        }

        return null;
    }
}
