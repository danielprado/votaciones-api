<?php

namespace IDRDApp\Http\Controllers\Votes;

use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Transformers\Votes\CandidateTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new Candidate(), new CandidateTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Votes\StoreCandidateRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Requests\Votes\StoreCandidateRequest $request)
    {
        $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('profile_pic')->getClientOriginalExtension();
        if ( $request->file('profile_pic')->move( public_path('storage/candidates'), $file_name  ) ) {
            $resource = new Candidate();
            $resource->name              =   $request->get('name');
            $resource->description       =   $request->get('profile');
            $resource->age               =   $request->get('age');
            $resource->campaign_id       =   $request->get('campaign_id');
            $resource->video_link        =   $request->get('video');
            $resource->profile_pic       =   $file_name;

            if ( $resource->saveOrFail() ) {
                return response()->json([
                    'data'  =>  trans('validation.handler.success'),
                    'code'  => 201
                ], 201);
            }
        }

        return response()->json([
            'data'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        $resource = new Item($candidate, new CandidateTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Votes\UpdateCandidateRequest $request
     * @param Candidate $candidate
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Requests\Votes\UpdateCandidateRequest $request, Candidate $candidate)
    {
        if ( candidateCanNotBeUpdatedOrDeleted( $candidate ) ) {
            return $this->error_response( trans('validation.handler.candidate_not_updated') );
        }
        
        if ( $request->hasFile( 'profile_pic' ) ) {

            if ( isset( $candidate->image_path) && file_exists( $candidate->image_path ) ) {
                unlink( $candidate->image_path );
            }

            $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('profile_pic')->getClientOriginalExtension();
            if ( $request->file('profile_pic')->move( public_path('storage/candidates'), $file_name  ) ) {
                $candidate->name              =   $request->get('name');
                $candidate->description       =   $request->get('profile');
                $candidate->age               =   $request->get('age');
                $candidate->campaign_id       =   $request->get('campaign_id');
                $candidate->video_link        =   $request->get('video');
                $candidate->profile_pic       =   $file_name;
            }

        } else {
            $candidate->name              =   $request->get('name');
            $candidate->description       =   $request->get('profile');
            $candidate->age               =   $request->get('age');
            $candidate->campaign_id       =   $request->get('campaign_id');
            $candidate->video_link        =   $request->get('video');
        }

        if ( $candidate->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'data'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Candidate $candidate
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Candidate $candidate)
    {
        if ( candidateCanNotBeUpdatedOrDeleted( $candidate ) ) {
            return $this->error_response( trans('validation.handler.candidate_not_deleted') );
        }

        if ( $candidate->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}
