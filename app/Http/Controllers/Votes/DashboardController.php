<?php

namespace IDRDApp\Http\Controllers\Votes;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Voter;

use IDRDApp\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function data()
    {
        $data = [
            'campaigns'     =>  Campaign::whereNull('deleted_at')->count(),
            'candidates'    =>  Candidate::whereNull('deleted_at')->count(),
            'voters'        =>  Voter::whereNull('deleted_at')->count(),
            'votes'         =>  DB::table('voters_campaigns')->whereNotNull('voted_at')->count(),
            'requested_at'  =>  Carbon::now()->toIso8601String()

        ];

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }
}
