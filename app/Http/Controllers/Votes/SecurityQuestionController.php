<?php

namespace IDRDApp\Http\Controllers\Votes;

use IDRDApp\Entities\Votes\SecurityQuestion;
use IDRDApp\Transformers\Votes\SecurityQuestionTransformer;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class SecurityQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  $this->table( new SecurityQuestion(), new SecurityQuestionTransformer()),
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Votes\StoreSecurityQuestionRequest $request
     * @return Response
     */
    public function store(Requests\Votes\StoreSecurityQuestionRequest $request)
    {
        $resource = new SecurityQuestion();
        $resource->security_question   =   $request->get('question');
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param SecurityQuestion $question
     * @return Response
     */
    public function show(SecurityQuestion $question)
    {
        $resource = new Item($question, new SecurityQuestionTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Votes\StoreSecurityQuestionRequest $request
     * @param SecurityQuestion $question
     * @return Response
     */
    public function update(Requests\Votes\StoreSecurityQuestionRequest $request, SecurityQuestion $question)
    {
        $question->security_question   =   $request->get('question');
        if ( $question->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SecurityQuestion $question
     * @return Response
     * @throws \Exception
     */
    public function destroy(SecurityQuestion $question)
    {
        if ( $question->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}
