<?php

namespace IDRDApp\Http\Controllers\Votes;

use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Team;
use IDRDApp\Http\Requests\Votes\StoreTeamRequest;
use IDRDApp\Http\Requests\Votes\UpdateTeamRequest;
use IDRDApp\Transformers\Votes\CandidateTransformer;

use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Votes\TeamTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class TeamController extends Controller
{
    public function index( Candidate $candidate )
    {
        $resource = new Collection($candidate->teams, new TeamTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTeamRequest $request
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeamRequest $request, Candidate $candidate)
    {
        $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('profile_pic')->getClientOriginalExtension();
        if ( $request->file('profile_pic')->move( public_path('storage/candidates'), $file_name  ) ) {

            $candidate->teams()->create([
                'name'              =>   $request->get('name'),
                'description'       =>   $request->get('profile'),
                'age'               =>   $request->get('age'),
                'candidate_id'      =>   $request->get('candidate_id'),
                'profile_pic'       =>   $file_name,
            ]);

             return response()->json([
                 'data'  =>  trans('validation.handler.success'),
                 'code'  => 201
             ], 201);
        }

        return response()->json([
            'data'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTeamRequest $request
     * @param Candidate $candidate
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateTeamRequest $request, Candidate $candidate, Team $team)
    {
        if ( candidateCanNotBeUpdatedOrDeleted( $candidate ) ) {
            return $this->error_response( trans('validation.handler.candidate_not_updated') );
        }

        if ( $request->hasFile( 'profile_pic' ) ) {

            if ( isset( $team->image_path) && file_exists( $team->image_path ) ) {
                unlink( $team->image_path );
            }

            $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('profile_pic')->getClientOriginalExtension();
            if ( $request->file('profile_pic')->move( public_path('storage/candidates'), $file_name  ) ) {
                $team->name              =   $request->get('name');
                $team->description       =   $request->get('profile');
                $team->age               =   $request->get('age');
                $team->candidate_id      =   $request->get('candidate_id');
                $team->profile_pic       =   $file_name;
            }

        } else {
            $team->name              =   $request->get('name');
            $team->description       =   $request->get('profile');
            $team->age               =   $request->get('age');
            $team->candidate_id      =   $request->get('candidate_id');
        }

        if ( $team->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'data'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Candidate $candidate
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Candidate $candidate, Team $team)
    {
        if ( candidateCanNotBeUpdatedOrDeleted( $candidate ) ) {
            return $this->error_response( trans('validation.handler.candidate_not_deleted') );
        }

        if ( $team->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  => 204
            ], 200);
        }
    }
}
