<?php

namespace IDRDApp\Http\Controllers\Votes;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Voter;
use IDRDApp\Transformers\Votes\VoterTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class VoterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->table( new Voter(), new VoterTransformer(), false);

        if ( \request()->has('campaign_id') ) {
            $data = $data->whereCampaign( \request()->get('campaign_id') );
        }
        $data = $this->paginateCollection( $data, new VoterTransformer() );

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Votes\StoreVoterRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function store(Requests\Votes\StoreVoterRequest $request)
    {
        //Verify if user exists in storage
        $resource = Voter::query()->where('document', $request->get('document'))->first();

        if ( isset( $resource->id ) ) {
            //Check if user has been assigned to specified campaign
            $campaign = $resource->campaigns()->where([
                ['campaign_id', $request->get('campaign_id') ],
                ['voter_id', $resource->id]
            ])->first();

            if ( isset( $campaign->id ) ) {
                return $this->error_response( trans('validation.handler.has_been_assigned', ['user' => $resource->name]), 422 );
            }
        } else {
            $resource = new Voter();
            $resource->name              =   $request->get('name');
            $resource->document          =   $request->get('document');
            $resource->sport             =   $request->get('sport');
            $resource->email             =   $request->get('email');
        }

        if ( $resource->saveOrFail() ) {


            $answer = isAValidDate( $request->get('security_answer') )
                ? Carbon::parse( $request->get('security_answer') )->format( 'Y-m-d' )
                : toUpper( $request->get('security_answer') ) ;

            $resource->campaigns()->attach( $resource->id, [ 'campaign_id' => $request->get('campaign_id') ] );
            $resource->security_questions()->attach($resource->id, [
                'campaign_id' => $request->get('campaign_id'),
                'security_question_id' => $request->get('security_question_id'),
                'security_answer' =>  $answer
            ]);

            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }
    }

    /**
     * Store a newly created resource in storage from excel file.
     *
     * @param Requests\Votes\StoreVoterWithFileRequest $request
     * @return Response
     */
    public function storeWithFile(Requests\Votes\StoreVoterWithFileRequest $request)
    {
        $saved = 0;
        $total = 0;
        $rejected = [];
        Excel::selectSheetsByIndex(0)->load( $request->file('excel')->getRealPath() , function ($file) use ($request, $saved, $total) {
            $file->setDateFormat('Y-m-d');
            foreach (  $file->get() as $row ) {
                if ( isset( $row->nombre, $row->documento, $row->respuesta, $row->deporte ) ) {

                    $resource = Voter::query()->where('document', toUpper( $row->documento ) )->first();
                    if ( isset( $resource->id ) ) {
                        $campaign = $resource->campaigns()->where([
                            ['campaign_id', $request->get('campaign_id') ],
                            ['voter_id', $resource->id]
                        ])->first();
                        if ( isset( $campaign->id ) ) {
                            $rejected[] = $row;
                        } else {
                            $saved++;
                            $this->saveExcelData( $resource, $request, $row, false );
                        }
                    } else {
                        $saved++;
                        $resource = new Voter();
                        $this->saveExcelData( $resource, $request, $row );
                    }
                } else {
                    $rejected[] = $row;
                }
                $total++;
            }
        });

        return response()->json([
            'data'      =>  "Se han almacenado $saved registros de un total de $total registros.",
            'details'   =>  $rejected,
            'code'      =>  201
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Voter $voter
     * @return Response
     */
    public function show(Voter $voter)
    {
        $resource = new Item($voter, new VoterTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Votes\UpdateVoterRequest $request
     * @param Voter $voter
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function update(Requests\Votes\UpdateVoterRequest $request, Voter $voter)
    {

        $campaign = $voter->campaigns()->where([
            ['campaign_id', $request->get('campaign_id') ],
            ['voter_id', $voter->id]
        ])->whereNull('voted_at')->first();

        if ( isset( $campaign->id ) ) {
            $voter->name              =   $request->get('name');
            $voter->document          =   $request->get('document');
            $voter->sport             =   $request->get('sport');
            $voter->email             =   $request->get('email');

            $answer = isAValidDate( $request->get('security_answer') )
                ? Carbon::parse( $request->get('security_answer') )->format( 'Y-m-d' )
                : toUpper( $request->get('security_answer') ) ;

            $voter->campaigns()->updateExistingPivot( $campaign->id, [
                'campaign_id' => $request->get('campaign_id'),
            ], false);

            $question = $voter->security_questions()->where([
                ['campaign_id', $request->get('campaign_id') ],
                ['voter_id', $voter->id]
            ])->first();

            if ( isset( $question->id ) ) {
                $voter->security_questions()->updateExistingPivot( $question->id, [
                    'campaign_id' => $request->get('campaign_id'),
                    'security_question_id' => $request->get('security_question_id'),
                    'security_answer' =>  $answer
                ], false);
            } else {
                $voter->security_questions()->attach($voter->id, [
                    'campaign_id' => $request->get('campaign_id'),
                    'security_question_id' => $request->get('security_question_id'),
                    'security_answer' =>  $answer
                ]);
            }

            if ( $voter->saveOrFail() ) {
                return response()->json([
                    'data'  =>  trans('validation.handler.updated'),
                    'code'  => 200
                ], 200);
            }
        }

        return $this->error_response( trans('validation.handler.has_voted', ['user' => $voter->name]), 422 );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Voter $voter
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Voter $voter)
    {
        if ( $voter->campaigns()->whereNotNull('voted_at')->count() < 1 ) {
            $voter->campaigns()->detach();
            $voter->security_questions()->detach();
            if ( $voter->forceDelete() ) {
                return response()->json([
                    'data'  =>  trans('validation.handler.deleted'),
                    'code'  => 204
                ], 200);
            }
        } else {
            return $this->error_response( trans('validation.handler.voter_not_deleted') );
        }
    }

    /**
     * Store or update new data in storage from excel data.
     *
     * @param  $resource
     * @param Request $request
     * @param $row
     * @param bool $update_voter
     */
    private function saveExcelData( $resource, Request $request, $row, $update_voter = true) {
        if ( $update_voter && isset( $row->nombre, $row->documento )) {
            $resource->name              =   toUpper( $row->nombre );
            $resource->document          =   toUpper( $row->documento );
            if ( !is_null( $row->deporte ) || !$row->deporte ) {
                $resource->sport             =   toUpper( $row->deporte );
            }
            if ( filter_var( $row->email, FILTER_VALIDATE_EMAIL ) ) {
                $resource->email             =   toLower($row->email);
            }
            $resource->saveOrFail();
        }
        $answer = isAValidDate( $row->respuesta ) ? Carbon::parse( $row->respuesta )->format( 'Y-m-d' ) : toUpper( $row->respuesta ) ;
        $resource->campaigns()->attach( $resource->id, [ 'campaign_id' => $request->get('campaign_id') ] );
        $resource->security_questions()->attach($resource->id, [
            'campaign_id' => $request->get('campaign_id'),
            'security_question_id' => $request->get('security_question_id'),
            'security_answer' =>  $answer
        ]);
    }

    /**
     * Remove the specified voter from campaign
     *
     * @param Requests\Votes\DetachVoterCampaignRequest $request
     * @param Voter $voter
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachVoterFromCampaign(Requests\Votes\DetachVoterCampaignRequest $request, Voter $voter )
    {
        $campaign = $voter->campaigns()->where([
            ['campaign_id', $request->get( 'campaign_id' )],
            ['voter_id', $voter->id],
        ])->whereNull('voted_at')->first();

        if ( isset( $campaign->pivot->id ) ) {
            if ( $voter->campaigns()->wherePivot('id', $campaign->pivot->id)->detach() ) {

                $question = $voter->security_questions()->where([
                    ['campaign_id', $request->get( 'campaign_id' )],
                    ['voter_id', $voter->id],
                ])->first();

                if ( isset( $question->pivot->id ) ) {
                    $voter->security_questions()->wherePivot('id',  $question->pivot->id)->detach();
                }

                return response()->json([
                    'data'  =>  trans('validation.handler.deleted'),
                    'code'  => 204
                ], 200);
            }
        }

        return $this->error_response( trans('validation.handler.voter_campaign_not_deleted') );
    }

    /**
     * Attach the specified campaign to voter
     *
     * @param Requests\Votes\AttachVoterCampaignRequest $request
     * @param Voter $voter
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachVoterToCampaign( Requests\Votes\AttachVoterCampaignRequest $request, Voter $voter )
    {
        $campaign = $voter->campaigns()->where([
            ['campaign_id', $request->get( 'campaign_id' )],
            ['voter_id', $voter->id]
        ]);

        if ( $campaign->count() < 1 ) {
            $voter->campaigns()->attach( $voter->id, [ 'campaign_id' => $request->get('campaign_id') ] );

            $answer = isAValidDate( $request->get('security_answer') )
                ? Carbon::parse( $request->get('security_answer') )->format( 'Y-m-d' )
                : toUpper( $request->get('security_answer') ) ;

            $voter->security_questions()->attach($voter->id, [
                'campaign_id' => $request->get('campaign_id'),
                'security_question_id' => $request->get('security_question_id'),
                'security_answer' =>  $answer
            ]);

            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 201
            ], 201);
        }

        return $this->error_response( trans('validation.handler.has_been_assigned') );
    }
}
