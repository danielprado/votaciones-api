<?php

namespace IDRDApp\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'message'   =>  trans('validation.handler.unauthenticated'),
                    'code'      =>  401
                ], 401);
            } else {
                return redirect()->guest('/');
            }
        }

        return $next($request);
    }
}
