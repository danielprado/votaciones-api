<?php

namespace IDRDApp\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check header request and determine localizaton
        $locale = ($request->hasHeader("X-Localization")) ? $request->header("X-Localization") : "es";
        // Set Laravel localization
        app()->setLocale($locale);

        return $next($request);
    }
}
