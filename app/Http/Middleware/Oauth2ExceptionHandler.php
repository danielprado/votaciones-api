<?php

namespace IDRDApp\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use League\OAuth2\Server\Exception\OAuthException;
use LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware;

class Oauth2ExceptionHandler extends OAuthExceptionHandlerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $response = $next($request);
            // Was an exception thrown? If so and available catch in our middleware
            if (isset($response->exception) && $response->exception) {
                throw $response->exception;
            }

            return $response;
        } catch (OAuthException $e) {
            $error = $e->getMessage();
            $code = $e->httpStatusCode;

            if ( $e->errorType == 'access_denied' ) {
                $error = trans('validation.handler.unauthorized');
                $code = 403;
            }

            if ( $e->errorType == 'invalid_request' ) {
                $error = trans('validation.handler.invalid_request');
                $code = 400;
            }

            if ( $e->errorType == 'invalid_client' ) {
                $error = trans('validation.handler.unauthorized');
                $code = 401;
            }

            return response()->json([
                'message' =>  $error,
                'type'    =>  $e->errorType,
                'details' =>  $e->getMessage(),
                'code'    =>  $code
            ], $code ); //->withHeaders( $e->getHttpHeaders() );

        }
    }
}
