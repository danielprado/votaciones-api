<?php

namespace IDRDApp\Http\Middleware;

use Carbon\Carbon;
use Closure;

class Signature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->headers->set( 'X-Application-Name', env('VUE_APP_FOOTER', 'Application Name')." - ".Carbon::now()->format('Y') );
        return $response;
    }
}
