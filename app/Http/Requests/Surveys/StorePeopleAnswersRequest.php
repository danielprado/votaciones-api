<?php

namespace IDRDApp\Http\Requests\Surveys;

use IDRDApp\Http\Requests\Request;

class StorePeopleAnswersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_id'     =>      'required|exists:surveys,id',
            'surveyed_id'   =>      'required|exists:surveyed_people,id',
            'answers'       =>      'required',
        ];
    }
}
