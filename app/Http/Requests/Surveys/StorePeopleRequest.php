<?php

namespace IDRDApp\Http\Requests\Surveys;

use IDRDApp\Http\Requests\Request;

class StorePeopleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required|min:3|max:191',
            'document'  =>  'required|min:3|max:15',
            'survey_id' =>  'required|numeric|exists:surveys,id'
        ];
    }
}
