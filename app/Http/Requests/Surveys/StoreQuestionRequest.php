<?php

namespace IDRDApp\Http\Requests\Surveys;

use IDRDApp\Http\Requests\Request;

class StoreQuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question'  =>  'required|min:3|max:6000',
            'name'      =>  'required|min:3|max:30',
            'question_type_id'  =>  'required|numeric|exists:question_types,id',
            'survey_id'  =>  'required|numeric|exists:surveys,id',
        ];
    }
}
