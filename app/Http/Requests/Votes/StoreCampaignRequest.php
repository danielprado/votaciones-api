<?php

namespace IDRDApp\Http\Requests\Votes;

use IDRDApp\Http\Requests\Request;

class StoreCampaignRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:3|max:80',
            'description'       =>  'required|min:3|max:2500',
            'available_from'    =>  'required|date|date_format:Y-m-d H:i:s',
            'available_until'   =>  'required|date|date_format:Y-m-d H:i:s',
            'results_at'        =>  'required|date|date_format:Y-m-d H:i:s',
        ];
    }
}
