<?php

namespace IDRDApp\Http\Requests\Votes;

use IDRDApp\Http\Requests\Request;

class StoreCandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required|min:3|max:80',
            'video'         =>  'min:3|max:255',
            'profile'       =>  'required|min:3|max:2500',
            'age'           =>  'required|date|date_format:Y-m-d',
            'profile_pic'   =>  'required|image',
            'campaign_id'   =>  'required|exists:campaigns,id',
        ];
    }
}
