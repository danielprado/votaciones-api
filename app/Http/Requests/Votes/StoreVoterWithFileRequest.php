<?php

namespace IDRDApp\Http\Requests\Votes;

use IDRDApp\Http\Requests\Request;

class StoreVoterWithFileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'excel'                 =>  'required|file|mimes:xls,xlsx',
            'security_question_id'  =>  'required|exists:security_questions,id',
            'campaign_id'           =>  'required|exists:campaigns,id',
        ];
    }
}
