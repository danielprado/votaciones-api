<?php

namespace IDRDApp\Http\Requests\Votes;

use IDRDApp\Http\Requests\Request;

class UpdateVoterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|min:3|max:80',
            'sport'  =>  'min:3|max:80',
            'email'                 =>  'required|email',
            'document'  =>  'required|unique:voters,document,'.$this->route('voter')->id,
            'security_answer'       =>  'required|min:3|max:80',
            'security_question_id'  =>  'required|exists:security_questions,id',
            'campaign_id'           =>  'required|exists:campaigns,id',
        ];
    }
}
