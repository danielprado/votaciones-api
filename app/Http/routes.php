<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::any('/', function () {
    return view('welcome');
})->name('welcome');


Route::post('oauth/access-token', 'Auth\AccessTokenController@index')->middleware('api')->name('oauth2-access-token');
Route::post('logout', 'Auth\AccessTokenController@logout')->middleware(['auth', 'oauth', 'api'])->name('logout');
Route::post('lock', 'Auth\AccessTokenController@lock')->middleware(['auth', 'oauth', 'api'])->name('lock');
Route::post('unlock', 'Auth\AccessTokenController@unlock')->middleware(['oauth', 'api'])->name('unlock');
Route::get('reconfirm/{voter}/{campaign}', 'Landing\VoteController@reconfirm')->name('reconfirm-mail');


Route::group(['prefix' =>   'api', 'middleware' => ['api']], function () {

    //Public Routes
    Route::group(['namespace' => 'Landing'], function () {
        Route::post('surveyed-people', 'PeopleController@store');
        Route::get('surveys-answers/{id}', 'PeopleAnswerController@index');
        Route::post('people-answers', 'PeopleAnswerController@store');
        Route::get('surveys/public', 'SurveyController@index');
        Route::get('surveys-available-next', 'SurveyController@next');
        Route::get('surveys/public/{survey}', 'SurveyController@show');
        Route::post('surveys/public/{survey}/validate', 'QuestionController@validateSurvey');

        Route::get('campaigns/public', 'CampaignController@index');
        Route::get('campaigns-available-next', 'CampaignController@next');
        Route::get('campaigns/public/{campaign}', 'CampaignController@show');
        Route::post('campaigns/{campaign}/voter', 'VoteController@validateVoter');
        Route::post('campaigns/{campaign}/voter-answers', 'VoteController@validateAnswers');
        Route::post('campaigns/{campaign}/vote', 'VoteController@vote');
        Route::get('campaigns/{campaign}/candidate/{candidate}', 'CampaignController@candidate');
    });
    //Auth routes
    Route::group(['middleware' => ['auth', 'oauth', 'api']], function () {
        Route::get('user', 'Security\AuthController@index')->name('auth.user');
        Route::group(['namespace' => 'Votes'], function () {
            Route::group(['prefix' => 'dashboard'], function () {
                Route::get('counters', 'DashboardController@data');
            });
            Route::get('campaigns/active', 'CampaignController@active');
            Route::post('campaigns/{campaign}/excel', 'CampaignController@excel');
            Route::resource('campaigns', 'CampaignController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'campaigns' => 'campaign' ]
            ]);
            Route::resource('candidates', 'CandidateController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'candidates' => 'candidate' ]
            ]);

            Route::resource('candidates.teams', 'TeamController', [
                'except' => ['create', 'edit'. 'show'],
                'parameters' => [ 'candidates' => 'candidate', 'teams' => 'team' ]
            ]);

            Route::resource('questions', 'SecurityQuestionController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'questions' => 'question' ]
            ]);
            Route::post('voters/file', 'VoterController@storeWithFile');
            Route::post('voters/{voter}/detach-campaign', 'VoterController@detachVoterFromCampaign');
            Route::post('voters/{voter}/attach-campaign', 'VoterController@attachVoterToCampaign');
            Route::resource('voters', 'VoterController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'voters' => 'voter' ]
            ]);
        });

        Route::group(['namespace' => 'Surveys'], function () {
            Route::get('question-types', 'QuestionTypeController@index');

            Route::get('surveys/active', 'SurveyController@active');
            Route::post('surveys/{survey}/excel', 'SurveyController@excel');
            Route::resource('surveys', 'SurveyController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'surveys' => 'survey' ]
            ]);
            Route::resource('survey-questions', 'QuestionController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'survey-questions' => 'question' ]
            ]);
            Route::resource('offered-answers', 'OfferedAnswerController', [
                'except' => ['create', 'edit'],
                'parameters' => [ 'offered-answers' => 'answer' ]
            ]);
        });
    });
});

