<?php

namespace IDRDApp\Jobs;

use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Voter;
use IDRDApp\Jobs\Job;
use IDRDApp\Transformers\Votes\VoterCampaignTransformer;
use IDRDApp\Transformers\Votes\VoterTransformer;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class ConfirmVoteMail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Voter
     */
    private $voter;
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * Create a new job instance.
     *
     * @param Voter $voter
     * @param Campaign $campaign
     */
    public function __construct(Voter $voter, Campaign $campaign)
    {
        $data = $campaign->voters()->wherePivot('voter_id', $voter->id)->first();
        $resource = new Item($data, new VoterCampaignTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        $this->voter = $rootScope->toArray()['data'];
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        if ( isset( $this->voter['email'] ) && filter_var( $this->voter['email'], FILTER_VALIDATE_EMAIL  ) ) {
            $mailer->send('mail.mail', ['voter' => $this->voter, 'campaign' => $this->campaign], function ($m)  {
                $m->from('mails@idrd.gov.co', 'Votaciones y Encuestas IDRD');
                $m->to( $this->voter['email'] , $this->voter['name'])->subject( $this->campaign->name );
            });
        }
    }
}
