<?php

namespace IDRDApp\Providers;

use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'candidate' =>  Candidate::class,
            'campaign'  =>  Campaign::class
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
