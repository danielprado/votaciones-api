<?php


namespace IDRDApp\Transformers\Security;


use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\TransformerAbstract;
use IDRDApp\Entities\Security\Profile;
use IDRDApp\Entities\Security\User;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $data
     * @return array
     */
    public function transform( User $data )
    {
        return [
            'id'        =>  isset( $data->id )       ? $data->id : null,
            'username'  =>  isset( $data->username ) ? $data->username : null,
            'profile'   =>  isset( $data->profile  ) ? $this->includeProfile($data) : []
        ];
    }

    /**
     * Include Profile
     *
     * @param User $data
     * @return array
     */
    public function includeProfile( User $data )
    {
        $manager = new Manager();
        $resource = $this->item( $data->profile, new ProfileTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}