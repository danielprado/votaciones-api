<?php


namespace IDRDApp\Transformers\Surveys;


use IDRDApp\Entities\Surveys\OfferedAnswer;
use League\Fractal\TransformerAbstract;

class AnswersTransformer extends TransformerAbstract
{
    public function transform(OfferedAnswer $data)
    {
        return [
            'id'                =>  isset( $data->id ) ? (int) $data->id : null,
            'answer'            =>  isset( $data->answer ) ? $data->answer : null,
            'count_answers'     =>  $data->people_answers()->count(),
            'question_id'       =>  isset( $data->question_id ) ? $data->question_id : null,
            'has_image'         =>  isset( $data->has_image )  ? $data->has_image : null,
            'src'               =>  isset( $data->path )  ?     asset("public/storage/surveys/$data->path") : null,
            'created_at'        =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}