<?php


namespace IDRDApp\Transformers\Surveys;


use Carbon\Carbon;
use IDRDApp\Entities\Surveys\AnswersView;
use IDRDApp\Entities\Surveys\Survey;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Transformers\Surveys\QuestionTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class LandingSurveyTransformer extends TransformerAbstract
{
    /**
     * @param Survey $data
     * @return array
     */
    public function transform(Survey $data)
    {
        return [
            'id'              =>  isset( $data->id ) ? (int) $data->id : null,
            'name'            =>  isset( $data->name ) ? $data->name : null,
            'description'     =>  isset( $data->description ) ?  $data->description : null,
            'available_from'  =>  isset( $data->available_from ) ? $data->available_from->format('Y-m-d H:i:s') : null,
            'available_until' =>  isset( $data->available_until ) ? $data->available_until->format('Y-m-d H:i:s') : null,
            'count_votes'     =>  isset( $data->id ) ? AnswersView::query()->where('survey_id', (int) $data->id )->count( DB::raw('DISTINCT surveyed_id') ) : 0,
            'can_vote'        =>  isset( $data->available_until ) ? ( Carbon::now()->lessThan( $data->available_until ) &&  Carbon::now()->greaterThanOrEqualTo( $data->available_from ) ) : false,
            'time_between_questions'    => isset( $data->time_between_questions ) ? (int) $data->time_between_questions : 0,
            'questions'       =>  isset( $data->questions ) ? $this->includeQuestions( $data ) : [],
            'results_at'      =>  isset( $data->results_at ) ? $data->results_at->toIso8601String() : null,
            'created_at'      =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'      =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'    =>  Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Include Candidates
     *
     * @param Survey $data
     * @return array
     */
    public function includeQuestions( Survey $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->questions, new QuestionTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}