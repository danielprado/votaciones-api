<?php


namespace IDRDApp\Transformers\Surveys;


use IDRDApp\Entities\Surveys\Question;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    public function transform(Question $data)
    {
        return [
            'id'                =>  isset( $data->id ) ? (int) $data->id : null,
            'question'          =>  isset( $data->question ) ? $data->question : null,
            'name'              =>  isset( $data->input_name ) ? $data->input_name : null,
            'question_type_id'  =>  isset( $data->question_type_id ) ? (int) $data->question_type_id : null,
            'survey_id'         =>  isset($data->survey_id) ? (int) $data->survey_id : null,
            'question_type_name'     =>  isset( $data->question_type->name ) ? $data->question_type->name : null,
            'question_type_description'     =>  isset( $data->question_type->display_name ) ? $data->question_type->display_name : null,
            'require_text'      =>  isset( $data->question_type->require_text ) ? (boolean) $data->question_type->require_text : false,
            'answers'           =>  isset( $data->answers ) ? $this->includeAnswers($data) : [],
            'created_at'        =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function includeAnswers( Question $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->answers, new AnswersTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}