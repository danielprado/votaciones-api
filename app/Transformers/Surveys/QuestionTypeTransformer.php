<?php


namespace IDRDApp\Transformers\Surveys;


use IDRDApp\Entities\Surveys\QuestionType;
use League\Fractal\TransformerAbstract;

class QuestionTypeTransformer extends TransformerAbstract
{
    public function transform( QuestionType $data )
    {
        return [
            'id'                =>  isset( $data->id ) ? (int) $data->id : null,
            'name'              =>  isset( $data->name ) ? $data->name : null,
            'display_name'      =>  isset( $data->display_name ) ? $data->display_name : null,
            'require_text'      =>  isset( $data->require_text ) ? (boolean) $data->require_text : false,
            'created_at'        =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}