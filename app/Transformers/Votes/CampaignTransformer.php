<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CampaignTransformer extends TransformerAbstract
{
    /**
     * @param Campaign $data
     * @return array
     */
    public function transform(Campaign $data)
    {
        return [
            'id'              =>  isset( $data->id ) ? (int) $data->id : null,
            'name'            =>  isset( $data->name ) ? $data->name : null,
            'description'     =>  isset( $data->description ) ?  $data->description : null,
            'count_votes'     =>  isset( $data->count_votes ) ?  $data->count_votes : null,
            'available_from'  =>  isset( $data->available_from ) ? $data->available_from->format('Y-m-d H:i:s') : null,
            'available_until' =>  isset( $data->available_until ) ? $data->available_until->format('Y-m-d H:i:s') : null,
            'can_vote'        =>  isset( $data->available_until ) ? ( Carbon::now()->lessThan( $data->available_until ) &&  Carbon::now()->greaterThanOrEqualTo( $data->available_from ) ) : false,
            'candidates'      =>  isset( $data->candidates ) ? $this->includeCandidates( $data ) : [],
            'votes'           =>  isset( $data->candidates ) ? $this->includeVotersWhereHasVoted( $data ) : [],
            'results_at'      =>  isset( $data->results_at ) ? $data->results_at->toIso8601String() : null,
            'created_at'      =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'      =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'    =>  Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Include Candidates
     *
     * @param Campaign $data
     * @return array
     */
    public function includeCandidates( Campaign $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->candidates, new CandidateTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }

    /**
     * Include Campaigns
     *
     * @param Voter $data
     * @return array
     */
    public function includeVotersWhereHasVoted( Campaign $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->voters()->wherePivot('voted_at', '!=', null)->get(), new VoterCampaignTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}