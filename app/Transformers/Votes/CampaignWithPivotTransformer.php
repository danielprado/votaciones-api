<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Campaign;
use IDRDApp\Entities\Votes\Candidate;
use League\Fractal\TransformerAbstract;

class CampaignWithPivotTransformer extends TransformerAbstract
{
    /**
     * @param Campaign $data
     * @return array
     */
    public function transform(Campaign $data)
    {
        return [
            'id'              =>  isset( $data->id ) ? (int) $data->id : null,
            'name'            =>  isset( $data->name ) ? $data->name : null,
            'candidate'       =>  isset( $data->pivot->candidate_id ) ? $this->includeCandidateName( $data->pivot->candidate_id ) : null,
            'candidate_id'    =>  isset( $data->pivot->candidate_id ) ? (int) $data->pivot->candidate_id : null,
            'voter_id'        =>  isset( $data->pivot->voter_id ) ? (int) $data->pivot->voter_id : null,
            'voted_at'        =>  isset( $data->pivot->voted_at ) ? Carbon::parse( $data->pivot->voted_at )->format('Y-m-d H:i:s') : null,
            'created_at'      =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'      =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'  =>  Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Get the candidate name
     *
     * @param null $id
     * @return mixed|null
     */
    public function includeCandidateName($id = null ) {
        if ( isset( $id ) ) {
            $data = Candidate::query()->find( $id );
            return isset( $data->name ) ? $data->name : null;
        }
        return null;
    }
}