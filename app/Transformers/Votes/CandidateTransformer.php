<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Candidate;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CandidateTransformer extends TransformerAbstract
{
    /**
     * @param Candidate $data
     * @return array
     */
    public function transform(Candidate $data)
    {
        return [
            'id'             =>  isset( $data->id ) ? (int) $data->id : null,
            'name'           =>  isset( $data->name ) ? $data->name : null,
            'profile'        =>  '', // isset( $data->description ) ?  $data->description : null,
            'age'            =>  null, // isset( $data->age ) ? $data->age->format('Y-m-d') : null,
            'count_votes'    =>  isset( $data->count_votes ) ? (int) $data->count_votes : 0,
            'profile_pic'    =>  isset( $data->profile_pic ) ? $data->profile_pic : null,
            'campaign_id'    =>  isset( $data->campaign_id ) ? (int) $data->campaign_id : null,
            'campaign'       =>  isset( $data->campaign_name ) ? $data->campaign_name : null,
            'can_vote'        =>  isset( $data->campaign->available_until ) ? ( Carbon::now()->lessThan( $data->campaign->available_until ) &&  Carbon::now()->greaterThanOrEqualTo( $data->campaign->available_from ) ) : false,
            'image'          =>  isset( $data->image ) ? $data->image : null,
            'video'          =>  null, //isset( $data->video_link ) ? $data->video_link : null,
            'team'           =>  isset( $data->teams ) ? $this->includeTeam( $data ) : [],
            'age_number'     =>  null, //isset( $data->age ) ? (int) $data->age->age : null,
            'created_at'     =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'     =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'   =>  Carbon::now()->toIso8601String()
        ];
    }

    public function includeTeam( Candidate $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->teams, new TeamTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}