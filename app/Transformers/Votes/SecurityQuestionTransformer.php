<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\SecurityQuestion;
use League\Fractal\TransformerAbstract;

class SecurityQuestionTransformer extends TransformerAbstract
{
    /**
     * @param SecurityQuestion $data
     * @return array
     */
    public function transform(SecurityQuestion $data)
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'question'      =>  isset( $data->security_question ) ? $data->security_question : null,
            'security_question'  =>  isset( $data->security_question ) ? $data->security_question : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'  =>  Carbon::now()->toIso8601String()
        ];
    }
}