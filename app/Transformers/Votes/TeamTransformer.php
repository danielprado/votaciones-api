<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Team;
use League\Fractal\TransformerAbstract;

class TeamTransformer extends TransformerAbstract
{
    /**
     * @param Team $data
     * @return array
     */
    public function transform(Team $data)
    {
        return [
            'id'             =>  isset( $data->id ) ? (int) $data->id : null,
            'name'           =>  isset( $data->name ) ? $data->name : null,
            'profile'        =>  isset( $data->description ) ?  $data->description : null,
            'age'            =>  isset( $data->age ) ? $data->age->format('Y-m-d') : null,
            'profile_pic'    =>  isset( $data->profile_pic ) ? $data->profile_pic : null,
            'image'          =>  isset( $data->image ) ? $data->image : null,
            'age_number'     =>  isset( $data->age ) ? (int) $data->age->age : null,
            'created_at'     =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'     =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null
        ];
    }
}