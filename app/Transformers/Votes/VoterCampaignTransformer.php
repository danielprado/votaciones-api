<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Candidate;
use IDRDApp\Entities\Votes\Voter;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class VoterCampaignTransformer extends TransformerAbstract
{
    /**
     * @param Voter $data
     * @return array
     */
    public function transform(Voter $data)
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'          =>  isset( $data->name ) ? $data->name : null,
            'document'      =>  isset( $data->document ) ? $data->document : null,
            'sport'         =>  isset( $data->sport ) ? $data->sport : null,
            'email'         =>  isset( $data->email ) ? $data->email : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'voted_at'      =>  isset( $data->pivot->voted_at ) ? $data->pivot->voted_at : null,
            'candidate_id'  =>  isset( $data->pivot->candidate_id ) ? (int) $data->pivot->candidate_id : null,
            'candidate'     =>  isset( $data->pivot->candidate_id ) ? $this->includeCandidateName(  $data->pivot->candidate_id ) : null,
        ];
    }

    public function includeCandidateName( $id = null )
    {
        if ( !is_null( $id ) ) {
            $candidate = Candidate::query()->where('id', $id)->first();
            return isset( $candidate->name ) ? $candidate->name : null;
        }

        return null;
    }
}