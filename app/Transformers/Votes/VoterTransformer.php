<?php


namespace IDRDApp\Transformers\Votes;


use Carbon\Carbon;
use IDRDApp\Entities\Votes\Voter;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class VoterTransformer extends TransformerAbstract
{
    /**
     * @param Voter $data
     * @return array
     */
    public function transform(Voter $data)
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'          =>  isset( $data->name ) ? $data->name : null,
            'document'      =>  isset( $data->document ) ? $data->document : null,
            'sport'         =>  isset( $data->sport ) ? $data->sport : null,
            'email'         =>  isset( $data->email ) ? $data->email : null,
            'campaigns'     =>  isset( $data->campaigns ) ? $this->includeCampaigns( $data ) : [],
            'data'          =>  isset( $data->campaigns ) ?  $data->campaigns : [],
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'requested_at'  =>  Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Include Campaigns
     *
     * @param Voter $data
     * @return array
     */
    public function includeCampaigns( Voter $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->campaigns, new CampaignWithPivotTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}