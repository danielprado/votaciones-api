<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->text('description')->nullable();
            $table->text('profile_pic');
            $table->date( 'age' );
            $table->unsignedInteger( 'campaign_id' );
            $table->timestamps();
            $table->softDeletes();

            /*
             * Foreign keys
             */
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
