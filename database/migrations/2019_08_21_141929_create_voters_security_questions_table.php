<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotersSecurityQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters_security_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voter_id');
            $table->unsignedInteger('security_question_id');
            $table->unsignedInteger('campaign_id');
            $table->text('security_answer');
            /*
             * Foreign keys
             */
            $table->foreign('voter_id')->references('id')->on('voters');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('security_question_id')->references('id')->on('security_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voters_security_questions');
    }
}
