<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotersCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('voter_id');
            $table->unsignedInteger('candidate_id');
            $table->timestamp('voted_at');
            /*
             * Unique Keys
             */
            $table->unique(['campaign_id', 'voter_id']);
            /*
             * Foreign keys
             */
            $table->foreign('voter_id')->references('id')->on('voters');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('candidate_id')->references('id')->on('candidates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voters_campaigns');
    }
}
