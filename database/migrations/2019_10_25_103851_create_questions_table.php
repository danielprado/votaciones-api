<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('question');
            $table->string('name', 30);
            $table->unsignedInteger('question_type_id');
            $table->unsignedBigInteger('survey_id');
            $table->timestamps();
            $table->foreign('question_type_id')
                  ->references('id')->on('question_types');
            $table->foreign('survey_id')
                ->references('id')->on('surveys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
