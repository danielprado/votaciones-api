<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSurveyAnswersViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("
            CREATE VIEW `{$db_name}`.`tbl_survey_answers_view` AS SELECT
                `tbl_people_answers`.`id` AS `id`,
                `tbl_surveys`.`id` AS `survey_id`,
                `tbl_surveys`.`name` AS `survey`,
                `tbl_people_answers`.`surveyed_id` AS `surveyed_id`,
                `tbl_surveyed_people`.`name` AS `name`,
                `tbl_surveyed_people`.`document` AS `document`,
                `tbl_questions`.`question` AS `question`,
                `tbl_people_answers`.`answer_id` AS `answer_id`,
                `tbl_offered_answers`.`answer` AS `answer`,
                `tbl_question_types`.`require_text` AS `require_text`,
                `tbl_people_answers`.`answer_text` AS `answer_text`,
                `tbl_question_types`.`display_name` AS `question_type`,
                `tbl_question_types`.`name` AS `question_type_name`,
                `tbl_people_answers`.`created_at` AS `created_at`,
                `tbl_people_answers`.`updated_at` AS `updated_at` 
            FROM
                (
                    (
                        (
                            (
                                ( `tbl_people_answers` JOIN `tbl_surveyed_people` ON ( ( `tbl_people_answers`.`surveyed_id` = `tbl_surveyed_people`.`id` ) ) )
                                JOIN `tbl_offered_answers` ON ( ( `tbl_people_answers`.`answer_id` = `tbl_offered_answers`.`id` ) ) 
                            )
                            JOIN `tbl_questions` ON ( ( `tbl_offered_answers`.`question_id` = `tbl_questions`.`id` ) ) 
                        )
                        JOIN `tbl_question_types` ON ( ( `tbl_question_types`.`id` = `tbl_questions`.`question_type_id` ) ) 
                    )
                JOIN `tbl_surveys` ON ( ( `tbl_questions`.`survey_id` = `tbl_surveys`.`id` ) ) 
                );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("DROP VIEW `{$db_name}`.`tbl_survey_answers_view`");
    }
}
