<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSurveyQuestionsViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("
            CREATE VIEW `{$db_name}`.`tbl_survey_questions_view` AS SELECT
                `tbl_surveys`.`id` AS `id`,
                `tbl_offered_answers`.`id` AS `answer_id`,
                `tbl_offered_answers`.`answer` AS `answer`,
                `tbl_questions`.`question` AS `question`,
                `tbl_questions`.`input_name` AS `input_name`,
                `tbl_question_types`.`name` AS `name`,
                `tbl_question_types`.`display_name` AS `display_name`,
                `tbl_question_types`.`require_text` AS `require_text`,
                `tbl_surveys`.`name` AS `survey` 
            FROM
                (
                    (
                        ( `tbl_offered_answers` JOIN `tbl_questions` ON ( ( `tbl_offered_answers`.`question_id` = `tbl_questions`.`id` ) ) )
                        JOIN `tbl_question_types` ON ( ( `tbl_questions`.`question_type_id` = `tbl_question_types`.`id` ) ) 
                    )
                JOIN `tbl_surveys` ON ( ( `tbl_questions`.`survey_id` = `tbl_surveys`.`id` ) ) 
                );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("DROP VIEW `{$db_name}`.`tbl_survey_questions_view`");
    }
}
