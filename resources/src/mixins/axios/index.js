/**
 * Mixin for loading states
 */

const AxiosInterceptorMixin = {
    install(Vue) {
        Vue.mixin({
            data: () => ({
                refCount: 0,
                isLoading: false,
            }),
            methods: {
                setLoading(isLoading) {
                    if (isLoading) {
                        this.refCount++;
                        this.isLoading = true;
                    } else if (this.refCount > 0) {
                        this.refCount--;
                        this.isLoading = this.refCount > 0;
                    }
                },
                axiosInterceptor: function() {
                    this.axios.interceptors.request.use(
                        config => {
                            this.setLoading(true);
                            return config;
                        },
                        error => {
                            this.setLoading(false);
                            return Promise.reject(error);
                        }
                    );

                    this.axios.interceptors.response.use(
                        response => {
                            this.setLoading(false);
                            return response;
                        },
                        error => {
                            this.setLoading(false);
                            return Promise.reject(error);
                        }
                    );
                }
            },
            mounted() {
                this.axiosInterceptor();
                const token = this.$store ? this.$store.getters['user/getToken'] : null;
                if (token) {
                    this.axios.defaults.headers.common['Authorization'] = token
                }
                this.axios.defaults.headers.common["X-Localization"] = this.$i18n ? this.$i18n.locale : 'es';
            }
        });
    }
};

export default AxiosInterceptorMixin;
