import {mapMutations} from "vuex";

/**
 * Mixin for custom features
 */

const FeaturesMixin = {
    install(Vue) {
        Vue.mixin({
            methods: {
                ...mapMutations('app', ['setDrawer', 'setDrawerColor', 'setImage', 'setColor', 'toggleDrawer', 'toggleLoading',
                    'toggleMini', 'toggleDark', 'toggleRTL', 'setDrawerRight', 'setFullScreenDialog',
                    'toggleDrawerRight', 'setSheet']),
                onDark: function () {
                    this.toggleDark();
                    this.$vuetify.theme.dark = this.darkTheme;
                },
                changeRTL () {
                    this.toggleRTL(!this.$vuetify.rtl);
                    this.$vuetify.rtl = !this.$vuetify.rtl;
                },
                onClickBtn () {
                    this.setDrawer(!this.$store.state.app.drawer)
                },
                onCloseFSD() {
                    this.setFullScreenDialog( false );
                },
                onShowFSD() {
                    this.setFullScreenDialog( true );
                },
                onClickSheet () {
                    this.setSheet(!this.$store.state.app.sheet)
                },
                onClickRight () {
                    this.setDrawerRight(!this.$store.state.app.drawerRight)
                },
            },
            computed: {
                darkTheme() {
                    return this.$store.state.app.dark
                },
                loadingPage: {
                    get() {
                        return this.$store.state.app.pageLoading
                    },
                    set() {
                        this.toggleLoading();
                    }
                },
                miniDrawer: {
                    get() {
                        return this.$store.state.app.mini
                    },
                    set() {
                        this.toggleMini();
                    }
                },
                accentColor() {
                    return this.$store.state.app.color
                },
                sideBarImage() {
                    return this.$store.state.app.image
                },
                sideBarColor() {
                    return this.$store.state.app.sidebarBackgroundColor
                },
                showDrawer: {
                    get () {
                        return this.$store.state.app.drawer
                    },
                    set (val) {
                        this.setDrawer(val)
                    }
                },
                showFullScreenDialog: {
                    get () {
                        return this.$store.state.app.fullScreenDialog
                    },
                    set (val) {
                        this.setFullScreenDialog(val)
                    }
                },
                showConsole: {
                    get () {
                        return this.$store.state.app.sheet
                    },
                    set (val) {
                        this.setSheet(val)
                    }
                },
                showDrawerRight: {
                    get () {
                        return this.$store.state.app.drawerRight
                    },
                    set (val) {
                        this.setDrawerRight(val)
                    }
                },
                viewRTL() {
                    return this.$store.state.app.rtl
                }
            },
            mounted () {
                if ( this.$store ) {
                    this.$vuetify.theme.dark = this.darkTheme;
                    this.$vuetify.rtl = this.viewRTL;
                }
            },
        });
    }
};

export default FeaturesMixin;
