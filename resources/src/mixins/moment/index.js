/**
 * Mixin for moment locale
 */

import moment from "moment";
window.moment = moment;
import i18n from "@/i18n";

const MomentMixin = {
  install(Vue) {
    Vue.mixin({
      data: () => ({
        moment: moment,
        current_time: moment(),
        right_now: null,
      }),
      mounted: function () {
        let that = this;
        this.setFromNow( i18n.locale);
        setInterval(function () {
          that.setFromNow( i18n.locale )
        }, 50000)
      },
      methods: {
        setFromNow: function (  locale = 'es' ) {
          this.right_now = this.moment( this.current_time ).locale( locale ).fromNow();
        },
      },
      filters: {
        chip_dates: function ( date, locale = 'es' ) {
          let format = locale === 'es' ? 'D MMM YY, h a' : 'MMM D YY, ha';
          return moment( date ).isValid() ? moment( date ).locale( locale ).format( format ) : date ;
        },
        card_title_dates: function ( date, locale = 'es', format = 'LL' ) {
          return moment( date ).isValid() ? moment( date ).locale( locale ).format( format ) : date ;
        },
        from_now: function (date, locale = 'es') {
          return moment( date ).isValid()
            ? moment( date ).locale( locale ).fromNow()
            : date;
        },
        to_now: function ( date, locale = 'es' ) {
          /*
          if ( moment(date).isValid() ) {
            return moment().isBefore( date )
                  ? moment().locale( locale ).to( date )
                  : moment().locale( locale ).calendar( date )
          }
          */
          
          return moment(date).isValid() ? moment().locale( locale ).to( date ) : date;
        },
      },
      watch: {
        '$i18n.locale': function () {
          this.moment().locale( this.$i18n.locale );
          moment().locale( this.$i18n.locale );
          this.setFromNow( this.$i18n.locale );
        }
      }
    })
  }
};

export default MomentMixin;