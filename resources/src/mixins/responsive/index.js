/**
 * Listener for resize window responsive
 */

const ResponsiveAndOrientationMixin = {
    install(Vue) {
        Vue.mixin({
            data: () => ({
                responsive: false,
                responsiveInput: false,
                screenLandscape: false
            }),
            methods: {
                onResponsiveInverted () {
                    if (window.innerWidth < 991) {
                        this.responsive = true;
                        this.responsiveInput = false;
                    } else {
                        this.responsive = false;
                        this.responsiveInput = true;
                    }
                },
                onChangeOrientation: function () {
                    if (  screen && screen.orientation ) {
                        this.screenLandscape = screen.orientation.angle === 90 && window.innerWidth < 991;
                    }
                },
            },
            mounted() {
                let that = this;
                this.onResponsiveInverted();
                window.addEventListener('resize', this.onResponsiveInverted)
                window.addEventListener("orientationchange", function () {
                    that.onChangeOrientation();
                }, false);
            },
            beforeDestroy() {
                window.removeEventListener("orientationchange", this.onChangeOrientation(), false);
                window.removeEventListener('resize', this.onResponsiveInverted)
            }
        });
    }
};

export default ResponsiveAndOrientationMixin;
