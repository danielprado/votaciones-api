export const Api = {
  END_POINTS: {
    //Project
    PUBLIC: () => `api`,
    DASHBOARD: () => `api/dashboard`,
    CAMPAIGNS: () => `api/campaigns`,
    CANDIDATES: () => `api/candidates`,
    TEAM: (id) => `api/candidates/${id}/teams`,
    QUESTIONS: () => `api/questions`,
    VOTERS: () => `api/voters`,

    //Surveys
    SURVEYS: () => `api/surveys`,
    SURVEY_QUESTION_TYPE: () => `api/question-types`,
    SURVEY_QUESTIONS: () => `api/survey-questions`,
    OFFERED_ANSWERS: () => `api/offered-answers`,
    ANSWERS: () => `api/answers`,
    SURVEYED_PEOPLE: () => `api/surveyed-people`,
    PEOPLE_ANSWERS: () => `api/people-answers`,

    // Auth Routes
    GET_PROFILE: () => `/api/user`,
    LOGIN: () => `/oauth/access-token`,
    LOGOUT: () => `/logout`,
    LOCK: () => `/lock`,
    UNLOCK: () => `/unlock`
  }
};
