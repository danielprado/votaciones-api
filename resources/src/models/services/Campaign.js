import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Campaign extends Model {
  constructor(data = {
    name: null,
    description: null,
    available_from: null,
    available_until: null,
    results_at: null,
  }) {
    super(Api.END_POINTS.CAMPAIGNS(), data);
  }
  
  public( options = {}, id = null ) {
    return id ? this.get( `${this.url}/public/${id}`, options ) : this.get( `${this.url}/public`, options )
  }

  excel( id, options = {}) {
    return this.post( `${this.url}/${id}/excel`, options );
  }
  
  onlyActive( options = {} ) {
    return this.get( `${this.url}/active`, options );
  }
  
  next(options = {}) {
    return this.get( `${this.url}-available-next`, options )
  }
}