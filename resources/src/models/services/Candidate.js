import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class Candidate extends Model {
  constructor( data = {
    name: null,
    profile: null,
    age: null,
    profile_pic: null,
    campaign_id: null,
    video: null,
  }) {
    super(Api.END_POINTS.CANDIDATES(), data);
  }
  
  candidate(campaign_id, candidate_id, options = {}) {
    return this.get( `${Api.END_POINTS.PUBLIC()}/campaigns/${campaign_id}/candidate/${candidate_id}`, options )
  }
  
  store( image ) {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      form_data.append( 'name', this.name );
      form_data.append( 'profile', this.profile );
      form_data.append( 'age', this.age );
      form_data.append( 'campaign_id', this.campaign_id );
      form_data.append( 'video', this.video );
      form_data.append( 'profile_pic', this.profile_pic, image.getChosenFile().name );
      
      vm.axios.post( this.url, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => image.remove() )
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })
      
    })
  }
  
  update( id, image = null ) {
    return new Promise( (resolve, reject) => {
      let config = {};
      let form_data = new FormData();
      form_data.append( 'name', this.name );
      form_data.append( 'profile', this.profile );
      form_data.append( 'video', this.video );
      form_data.append( 'age', this.age );
      form_data.append( 'campaign_id', this.campaign_id );
      form_data.append( '_method', 'PUT');
      
      if ( image && image.hasImage() ) {
         config = {
          header : {
            'Content-Type' : 'multipart/form-data'
          }
        };
  
        if ( image.hasImage() ) {
          form_data.append( 'profile_pic', this.profile_pic, image.getChosenFile().name );
        }
      } else {
        form_data.append( 'profile_pic', this.profile_pic );
      }
    
      vm.axios.post( `${this.url}/${id}`, form_data, config )
        .then((response) => resolve( response.data ))
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })
    
    })
  }
  
}