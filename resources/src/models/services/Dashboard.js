import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Dashboard extends Model {
  constructor() {
    super(Api.END_POINTS.DASHBOARD(), {});
  }
  
  counters( options = {} ) {
    return this.get( `${this.url}/counters`, options )
  }
  
}