import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class OfferedAnswer extends Model {
  constructor(data = {
    question_id: null,
    answer: null,
    image: null,
    file_name: null
  }) {
    super(Api.END_POINTS.OFFERED_ANSWERS(), data);
  }
  
  store(options = {}) {
    return this.image ? this.storeWithImage() : super.store(options);
  }
  
  update(id, options = {}) {
    return this.image ? this.storeWithImage() : super.update(id, options);
  }
  
  storeWithImage() {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      form_data.append( 'question_id', this.question_id );
      form_data.append( 'answer', this.answer );
      form_data.append( 'image', this.image, this.file_name );
      
      if ( this.id ) {
        form_data.append( '_method', 'PUT' );
      }
      
      vm.axios.post( this.url, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => this.image.remove() )
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })
      
    })
  }
}