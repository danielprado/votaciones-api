import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class PeopleAnswers extends Model {
  constructor(data = {
    survey_id: null,
    surveyed_id: null,
    answers: {},
  }) {
    super(Api.END_POINTS.PEOPLE_ANSWERS(), data);
  }

  survey(id, options = {}) {
    return this.get( `${Api.END_POINTS.PUBLIC()}/surveys-answers/${id}`, options )
  }
}