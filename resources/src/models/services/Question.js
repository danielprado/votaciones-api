import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Question extends Model {
  constructor(data = {
    question: null,
    name: null,
    question_type_id: null,
    survey_id: null,
  }) {
    super(Api.END_POINTS.SURVEY_QUESTIONS(), data);
  }
}