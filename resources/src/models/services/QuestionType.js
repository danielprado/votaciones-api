import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class QuestionType extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.SURVEY_QUESTION_TYPE(), data);
  }
}