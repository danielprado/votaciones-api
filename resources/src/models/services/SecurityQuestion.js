import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class SecurityQuestion extends Model {
  constructor(data = {
    question: null
  }) {
    super( Api.END_POINTS.QUESTIONS(), data );
  }
}