import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class SurveyedPerson extends Model {
  constructor(data = {
    name: null,
    document: null,
    survey_id: null
  }) {
    super(Api.END_POINTS.SURVEYED_PEOPLE(), data);
  }

}