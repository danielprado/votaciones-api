import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class Team extends Model {
  constructor( id = 0, data = {
    name: null,
    profile: null,
    age: null,
    profile_pic: null,
  }) {
    super(Api.END_POINTS.TEAM(id), data);
  }

  store( image ) {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      form_data.append( 'name', this.name );
      form_data.append( 'profile', this.profile );
      form_data.append( 'age', this.age );
      form_data.append( 'candidate_id', this.candidate_id );
      form_data.append( 'profile_pic', this.profile_pic, image.getChosenFile().name );

      vm.axios.post( this.url, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => image.remove() )
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })

    })
  }

  update( id, image = null ) {
    return new Promise( (resolve, reject) => {
      let config = {};
      let form_data = new FormData();
      form_data.append( 'name', this.name );
      form_data.append( 'profile', this.profile );
      form_data.append( 'age', this.age );
      form_data.append( 'candidate_id', this.candidate_id );
      form_data.append( '_method', 'PUT');

      if ( image && image.hasImage() ) {
        config = {
          header : {
            'Content-Type' : 'multipart/form-data'
          }
        };

        if ( image.hasImage() ) {
          form_data.append( 'profile_pic', this.profile_pic, image.getChosenFile().name );
        }
      } else {
        form_data.append( 'profile_pic', this.profile_pic );
      }

      vm.axios.post( `${this.url}/${id}`, form_data, config )
        .then((response) => resolve( response.data ))
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })

    })
  }

}