import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class ValidateSurvey extends Model {
  constructor(data = {
    pin: '1DRD01',
  }) {
    super(Api.END_POINTS.PUBLIC(), data);
  }

  validateSurvey( id, options = {} ) {
    return this.post( `${this.url}/surveys/public/${id}/validate`, options )
  }
}