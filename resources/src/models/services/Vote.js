import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Vote extends Model {
  constructor(data = {
    document: null,
    selected_answers: [],
    voter_id: null,
    candidate_id: null
  }) {
    super(Api.END_POINTS.PUBLIC(), data);
  }
  
  check(options = {}) {
    return this.post( `${this.url}/check-campaign`, options )
  }
  
  validateVoter( id, options = {} ) {
    return this.post( `${this.url}/campaigns/${id}/voter`, options )
  }
  
  validateAnswers( id,  options = {}) {
    return this.post( `${this.url}/campaigns/${id}/voter-answers`, options )
  }

  vote( id, options = {}) {
    return this.post( `${this.url}/campaigns/${id}/vote`, options )
  }
  
}