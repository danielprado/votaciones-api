import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class Voter extends Model {
  constructor(data = {
    excel: null,
    name: null,
    document: null,
    sport: null,
    email: null,
    security_answer: null,
    campaign_id: null,
    security_question_id: null
  }) {
    super(Api.END_POINTS.VOTERS(), data);
  }

  storeWithFile( ) {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      form_data.append( 'excel', this.excel);
      form_data.append( 'campaign_id', this.campaign_id );
      form_data.append( 'security_question_id', this.security_question_id );

      vm.axios.post( `${this.url}/file`, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })

    })
  }
  
  detachFromCampaign( id, options = {} ) {
    return this.post( `${this.url}/${id}/detach-campaign`,  options )
  }
  
  attachToCampaign( id, options = {} ) {
    return this.post( `${this.url}/${id}/attach-campaign`,  options )
  }

}