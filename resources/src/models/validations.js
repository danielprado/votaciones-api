export default {
  required: {
    required: true,
  },
  text_required: {
    required: true,
    min: 3,
    max: 2500
  },
  excel_file_required: {
    required: true,
    mimes: ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
  },
  text: {
    min: 3,
    max: 2500
  },
  input_text_required: {
    required: true,
    min: 3,
    max: 80
  },
  input_text: {
    min: 3,
    max: 80
  },
  input_email_required: {
    required: true,
    email: true,
    min: 3,
    max: 191
  },
  input_email: {
    email: true,
    min: 3,
    max: 191
  },
  input_number_required: {
    required: true,
    numeric: true,
  },
  input_number_between_required: (min = 0, max = 30) => ({
    required: true,
    numeric: true,
    min: min,
    max: max
  }),
  input_number: {
    numeric: true,
  },
  input_phone_required: {
    required: true,
    numeric: true,
    digits: 7,
  },
  input_phone: {
    numeric: true,
    digits: 7,
  },
  pin: {
    alpha_num: true,
    min: 6,
    max: 6
  },
  pin_required: {
    required: true,
    alpha_num: true,
    min: 6,
    max: 6
  },
  input_mobile_required: {
    required: true,
    numeric: true,
    digits: 10,
  },
  input_mobile: {
    numeric: true,
    digits: 10,
  },
  input_document_required: {
    required: true,
    numeric: true,
    min: 3,
    max: 12
  },
  input_document: {
    numeric: true,
    min: 3,
    max: 12
  },
  input_date_required: {
    required: true,
    date_format: 'yyyy-MM-dd'
  },
  input_date: {
    date_format: 'yyyy-MM-dd'
  },
  input_datetime_required: {
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
  },
  input_datetime: {
    date_format: 'yyyy-MM-dd HH:mm:ss',
  },
  input_datetime_after_required: ( target ) => ({
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
    after: target
  }),
  input_datetime_before_required: ( target ) => ({
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
    before: target
  })
}