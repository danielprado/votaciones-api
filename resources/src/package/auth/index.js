import store from "@/store";
import {User} from "@/models/services/User";

export default function(Vue) {
    Vue.auth = {
      login: ( data ) => {
        return new Promise( (resolve, reject) =>  {
          (new User({
            grant_type: process.env.VUE_APP_GRANT_TYPE,
            client_id: process.env.VUE_APP_CLIENT_ID,
            client_secret: process.env.VUE_APP_CLIENT_SECRET,
            ...data
          })).login()
            .then((response) => {
              store.dispatch('user/login', response.data)
                    .then(() => resolve( response ) )
            })
            .catch( error => reject( error ) )
        })
      },
      logout: () => {
        return new Promise((resolve, reject) => {
          (new User({})).logout()
            .then( response => {
              store.dispatch('user/logout')
                .then(() => resolve( response ))
            })
            .catch( error => reject( error ) )
            .finally(() => store.dispatch('user/logout'))
        })
      },
      lock: () => {
        return new Promise((resolve, reject) => {
          (new User({})).lock()
            .then( response => resolve(response))
            .catch( error => {
              store.dispatch('user/logout')
                .then(() => reject( error ) );
            })
        })
      },
      unlock: (data) => {
        return new Promise((resolve, reject) => {
          (new User({
            grant_type: process.env.VUE_APP_GRANT_TYPE,
            client_id: process.env.VUE_APP_CLIENT_ID,
            client_secret: process.env.VUE_APP_CLIENT_SECRET,
            ...data
          })).unlock()
            .then( response => resolve(response))
            .catch( error => {
              store.dispatch('user/logout')
                .then(() => reject( error ) );
            })
        })
      },
      ready: () => !!store,
      isLoggedIn: () => !!store.getters['user/isLoggedIn'],
      isNotLoggedIn () { return  !this.isLoggedIn() },
    },
    Object.defineProperties(Vue.prototype, {
      $auth: {
        get() {
          return Vue.auth;
        }
      }
    });
}