import Vue from "vue"
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

let protocol = window.location.protocol;
let host = window.location.hostname;
let url = `${protocol}//${host}/SIM/votaciones/`;

Vue.axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? process.env.VUE_APP_API_URL_DEV : url;
Vue.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
Vue.axios.defaults.headers.common["Accept"] = "application/json";
Vue.axios.defaults.headers.common["Content-Type"] = "application/json";

window.CancelToken = Vue.axios.CancelToken;
window.source = null;
