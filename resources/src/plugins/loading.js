import Vue from "vue";
import { VclCode, VclTable, VclFacebook, VclInstagram, VclList} from "vue-content-loading";
import VueContentLoading from "vue-content-loading";

Vue.component( 'vcl-code', VclCode );
Vue.component( 'vcl-table', VclTable );
Vue.component( 'vcl-facebook', VclFacebook );
Vue.component( 'vcl-instagram', VclInstagram );
Vue.component( 'vcl-list', VclList );
Vue.component( 'vue-content-loading', VueContentLoading );