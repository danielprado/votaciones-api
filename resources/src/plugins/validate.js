import Vue from 'vue'
import VeeValidate, { Validator } from "vee-validate";
import es from "vee-validate/dist/locale/es";
import en from "vee-validate/dist/locale/en";
Validator.localize("es", es);

const attributes = {
  username: 'usuario',
  password: 'contraseña',
  name: 'nombre',
  description: 'descripción',
  available_from: 'disponible desde',
  available_until: 'disponible hasta',
  profile: 'perfil',
  profile_pic: 'foto de perfil',
  age: 'edad',
  campaign: 'campaña',
  campaign_id: 'campaña',
  candidates: 'candidatos',
  avatar: 'avatar',
  question: 'pregunta',
  security_answer: 'respuesta',
  document: 'documento',
  security_question_id: 'pregunta',
};

Vue.use(VeeValidate, {
  locale: "es",
  dictionary: {
    es: {
      messages: es,
      attributes: attributes
    },
    en
  }
});

/*
  this.errors[key][0]
  fieldsBagName: "veeFields",
  locale: "es"
 */