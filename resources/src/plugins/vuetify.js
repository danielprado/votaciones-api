import '@/assets/styles/index.scss'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import es from 'vuetify/es5/locale/es';
import theme from './theme';
import TownIcon from '@/components/icons/TownHall';
import Es from '@/components/icons/Es';
import En from '@/components/icons/En';
import FlipHorizontal from '@/components/icons/FlipHorizontal';
import FlipVertical from '@/components/icons/FlipVertical';
import Bogota from '@/components/icons/Bogota';
import LogoBogota from '@/components/icons/Logo';
import IDRD from '@/components/icons/IDRD';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: theme,
  },
    lang: {
      locales: { es },
      current: 'es',
    },
  icons: {
    iconfont: 'mdi',
    values: {
      idrd: {
        component: IDRD
      },
      town: {
        component: TownIcon
      },
      bogota: {
        component: Bogota
      },
      logo_bogota: {
        component: LogoBogota
      },
      es: {
        component: Es
      },
      en: {
        component: En
      },
      flip_horizontal: {
        component: FlipHorizontal
      },
      flip_vertical: {
        component: FlipVertical
      },
    }
  },
});
