/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
import Vue from 'vue'
import User from "@/package/user";
import Auth from "@/package/auth";
Vue.use(User);
Vue.use(Auth);

export default [
    {
        path: '/landing',
        name: 'Landing',
        view: 'Landing',
        icon: 'mdi-home',
        hideInMenu: false,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: false,
        }
    },
    {
        path: '/campaign/:id/vote',
        name: 'Vote',
        view: 'Vote',
        icon: 'mdi-vote-outline',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: true,
            hideFooter: false,
            isInset: true,
        }
    },
    {
        path: '/campaign/:campaign_id/candidate/:candidate_id',
        name: 'Profile',
        view: 'Profile',
        icon: 'mdi-account',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: false,
        }
    },
    {
        path: '/campaign/:id/vote/candidates',
        name: 'VoteCandidates',
        view: 'VoteGrid',
        icon: 'mdi-vote-outline',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: true,
            isInset: true,
        }
    },
    {
        path: '/campaign/:id',
        name: 'Campaign',
        view: 'Campaign',
        icon: 'mdi-home',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: false,
        }
    },
    {
        path: '/campaign/:id/thank-you',
        name: 'ThankYou',
        view: 'ThankYou',
        icon: 'mdi-home',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: true,
            isInset: true,
        }
    },
    {
        path: '/login',
        name: 'Login',
        view: 'Login',
        icon: 'mdi-fingerprint',
        hideInMenu: Vue.auth.isLoggedIn(),
        meta: {
            isInset: true,
            absoluteFooter: true,
            hideFooter: false,
        }
    },
    {
        path: '/lock',
        name: 'Lock',
        view: 'Lock',
        icon: 'mdi-lock',
        hideInMenu: true,
        meta: {
            forVisitors: undefined,
            requiresAuth: true,
            isInset: true,
            absoluteFooter: true,
            hideFooter: false,
        }
    },
    {
        path: '/surveys/:id',
        name: 'Survey',
        view: 'Survey',
        icon: 'mdi-home',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: false,
        }
    },
    {
        path: '/surveys/:id/start',
        name: 'StartSurvey',
        view: 'StartSurvey',
        icon: 'mdi-vote-outline',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: true,
            hideFooter: false,
            isInset: true,
        }
    },
    {
        path: '/surveys/:id/results',
        name: 'Results',
        view: 'SurveyResults',
        icon: 'mdi-vote-outline',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: false,
        }
    },
    {
        path: '/surveys/:id/thank-you',
        name: 'ThankYouSurvey',
        view: 'ThankYouSurvey',
        icon: 'mdi-vote-outline',
        hideInMenu: true,
        props: true,
        meta: {
            forVisitors: false,
            absoluteFooter: false,
            hideFooter: true,
            isInset: true,
        }
    },
]

/*
{
        path: '/pricing',
        name: 'Pricing',
        view: 'Pricing',
        icon: 'mdi-bank',
        hideInMenu: false,
        meta: {
            forVisitors: false,
            isInset: true,
            absoluteFooter: true
        }
    },
    {
        path: '/lock',
        name: 'Lock',
        view: 'Lock',
        icon: 'mdi-lock',
        hideInMenu: false,
        meta: {
            forVisitors: false,
            isInset: true,
            absoluteFooter: true
        }
    },
 */