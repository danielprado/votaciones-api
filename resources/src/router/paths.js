/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
    {
        path: '/home',
        name: 'Home',
        view: 'Home',
        icon: 'mdi-view-dashboard',
        can: true
    },
    {
        path: '/campaigns',
        name: 'Campaigns',
        view: 'Campaigns',
        icon: 'mdi-ballot',
        can: true
    },
    {
        path: '/candidates',
        name: 'Candidates',
        view: 'Candidates',
        icon: 'mdi-account-group',
        can: true
    },
    {
        path: '/security-questions',
        name: 'Security Questions',
        view: 'SecurityQuestion',
        icon: 'mdi-help-circle',
        can: true
    },
    {
        path: '/voters',
        name: 'Voters',
        view: 'Voters',
        icon: 'mdi-vote-outline',
        can: true,
    },
    {
        path: '/surveys',
        name: 'Surveys',
        view: 'Surveys',
        icon: 'mdi-ballot',
        can: true
    },
]

/*
{
        path: '/home',
        name: 'Home',
        view: 'Home',
        icon: 'mdi-view-dashboard',
        can: true
    },
    {
        path: '/about',
        name: 'About',
        view: 'About',
        icon: 'mdi-help-box',
        can: true
    },
    {
        path: '/charts',
        name: 'Charts',
        view: 'Charts',
        icon: 'mdi-chart-timeline-variant',
        can: true
    },
    {
        path: '/tabs',
        name: 'Tabs',
        view: 'Tabs',
        icon: 'mdi-bell',
        can: true
    },
    {
        path: '/users',
        name: 'Users',
        icon: 'mdi-account-circle',
        can: true,
        children: [
            {
                path: 'create',
                name: 'Create Users',
                view: 'Widgets',
                can: true
            },
            {
                path: 'about',
                name: 'About Users',
                view: 'Wizard',
                can: true
            },
        ]
    },
 */