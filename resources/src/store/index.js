import Vue from 'vue'
import Vuex from 'vuex'
import localForage from "localforage";
import VuexPersist from "vuex-persist";

// Store functionality
import modules from './modules'

Vue.use(Vuex)

const vuexPersistEmitter = () => {
  return store => {
    store.subscribe(mutation => {
      if (mutation.type === "RESTORE_MUTATION") {
        store._vm.$root.$emit("storageReady");
      }
    });
  };
};

const debug = process.env.NODE_ENV !== "production";

localForage.config({
  name: process.env.MIX_LOCAL_DB_NAME || 'new_application',
  version: 1.0,
  storeName: process.env.MIX_LOCAL_DB_NAME || 'new_application',
  description: "Almacena de forma offline algunos atributos de la base de datos."
});

const vuexLocalStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME || 'new_application',
  storage: localForage
  //modules: ['app']
});

const vuexLocalStorageSetting = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME  || 'new_application',
  storage: window.localStorage,
  modules: ["app"]
});

const vuexSessionStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME  || 'new_application',
  storage: window.sessionStorage,
  modules: ["user"]
});

// Create a new store
const store = new Vuex.Store({
  mutations: {
    RESTORE_MUTATION: vuexLocalStorage.RESTORE_MUTATION // this mutation **MUST** be named "RESTORE_MUTATION"
  },
  modules: {
    ...modules
  },
  plugins: [
    vuexPersistEmitter(),
    vuexLocalStorage.plugin,
    vuexLocalStorageSetting.plugin,
    vuexSessionStorage.plugin
  ],
  strict: debug
})


export default store
