import validations from "@/models/validations";
export default {
  text: {
    type: 'text',
    icon: 'mdi-dots-horizontal',
    validations: validations.input_text,
    counter: validations.input_text.max,
    maxlength: validations.input_text.max,
  },
  text_required: {
    type: 'text',
    icon: 'mdi-dots-horizontal',
    validations: validations.input_text_required,
    counter: validations.input_text_required.max,
    maxlength: validations.input_text_required.max,
  },
  long_text: {
    type: 'textarea',
    icon: 'mdi-text',
    validations: validations.text,
    counter: validations.text.max,
    maxlength: validations.text.max,
  },
  long_text_required: {
    type: 'textarea',
    icon: 'mdi-text',
    validations: validations.text_required,
    counter: validations.text_required.max,
    maxlength: validations.text_required.max,
  },
  email: {
    type: 'email',
    icon: 'mdi-email',
    validations: validations.input_email,
    counter: validations.input_email.max,
    maxlength: validations.input_email.max,
  },
  email_required: {
    type: 'email',
    icon: 'mdi-email',
    validations: validations.input_email_required,
    counter: validations.input_email_required.max,
    maxlength: validations.input_email_required.max,
  },
  document: {
    type: 'text',
    icon: 'mdi-account-card-details',
    validations: validations.input_document,
    counter: validations.input_document.max,
    maxlength: validations.input_document.max,
  },
  document_required: {
    type: 'text',
    icon: 'mdi-account-card-details',
    validations: validations.input_document_required,
    counter: validations.input_document_required.max,
    maxlength: validations.input_document_required.max,
  },
  phone: {
    type: 'text',
    icon: 'mdi-phone',
    validations: validations.input_phone,
    counter: validations.input_phone.digits,
    maxlength: validations.input_phone.digits,
  },
  phone_required: {
    type: 'text',
    icon: 'mdi-phone',
    validations: validations.input_phone_required,
    counter: validations.input_phone_required.digits,
    maxlength: validations.input_phone_required.digits,
  },
  mobile: {
    type: 'text',
    icon: 'mdi-cellphone-iphone',
    validations: validations.input_mobile,
    counter: validations.input_mobile.digits,
    maxlength: validations.input_mobile.digits,
  },
  mobile_required: {
    type: 'text',
    icon: 'mdi-cellphone-iphone',
    validations: validations.input_mobile_required,
    counter: validations.input_mobile_required.digits,
    maxlength: validations.input_mobile_required.digits,
  },
  numeric: {
    type: 'number',
    icon: 'mdi-numeric',
    min: 0,
    validations: validations.input_number,
  },
  numeric_required: {
    type: 'number',
    icon: 'mdi-numeric',
    min: 0,
    validations: validations.input_number_required,
  },
  boolean: {
    type: 'boolean',
    validations: validations.required,
  },
  options: {
    type: 'options',
    validations: validations.input_number,
    values: []
  },
  options_required: {
    type: 'options',
    validations: validations.input_number_required,
    values: []
  },
  options_multiple: {
    type: 'options_multiple',
    validations: validations.input_number,
    values: []
  },
  options_multiple_required: {
    type: 'options_multiple',
    validations: validations.input_number_required,
    values: []
  },
  images: {
    type: 'images',
    validations: validations.input_number,
    values: []
  },
  images_required: {
    type: 'images',
    validations: validations.input_number_required,
    values: []
  },
  images_multiple: {
    type: 'images_multiple',
    validations: validations.input_number,
    values: []
  },
  images_multiple_required: {
    type: 'images_multiple',
    validations: validations.input_number_required,
    values: []
  },
  rate: {
    type: 'rate',
    min: 1,
    validations: validations.input_number,
  },
  rate_required: {
    type: 'rate',
    min: 1,
    validations: validations.input_number_required,
  }
}