const config = require('./webpack.config');

module.exports = {
  publicPath: (process.env.NODE_ENV === "development")
    ? process.env.VUE_APP_PUBLIC_PATH_DEV
    : process.env.VUE_APP_PUBLIC_PATH_PROD,
  productionSourceMap: process.env.NODE_ENV !== 'production',
  outputDir: __dirname + "/public/",
  integrity: true,
  configureWebpack: { ...config },
  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'es',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  pwa: {
    name: process.env.VUE_APP_FOOTER || "IDRD",
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    themeColor: '#0f1b2e',
    msTileColor: '#0f1b2e',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: __dirname +  '/resources/src/sw.js',
      swDest: 'service-worker.js',
      exclude: [/\.htaccess$/, /\.txt$/, /\.config$/, /\.xlsx/]
    },
  },
  devServer: {
    //proxy: 'http://votaciones.test',
    //host: 'votaciones.test',
    //port: 8080,
    //hot: true,
    disableHostCheck: true,
    inline: false,
    writeToDisk: true
  }
};
