module.exports = {
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
            vue$: "vue/dist/vue.esm.js",
            "@": __dirname + "/resources/src"
        }
    },
    entry: {
        app: __dirname + '/resources/src/main.js'
    }
}